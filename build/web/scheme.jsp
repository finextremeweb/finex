<%-- 
    Document   : scheme
    Created on : Sep 10, 2015, 9:11:39 AM
    Author     : Simz
--%>

<%@taglib uri="/struts-tags" prefix="s" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<script type="text/javascript">
    function submit() {
        document.forms[0].submit();
    }
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        <link href="assets/css/bootstrap-united.css" rel="stylesheet" />
        <link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet" />
        <style>
            body {
                height: 100%;
                margin: 0;	
                background-size: 1440px 800px;
                background-repeat: no-repeat;
                display: compact;
            }
            myHalfCol .col-lg-6{width:48%; margin:0 1% /* or auto */;}
        </style>
    </head>
    <body >
         <s:actionerror/>
        <div>
            <s:form action="regScheme">
                <s:textfield name="schemeNo" label="Scheme No"></s:textfield>
                <s:textfield name="schemeName" label="Scheme Name"></s:textfield>
                <s:textfield name="county" label="County"></s:textfield>
                <s:textfield name="subCounty" label="Sub County"></s:textfield>
                <s:select label="Scheme Size" 
                                headerKey="1" headerValue="--- Select ---"
                                list="sizes" 
                                name="mySizes" ></s:select>
                   <s:textfield name="buyingPrice" label="Buying Price"></s:textfield>
                <sx:datetimepicker name="purchaseDate" label="Purchase Date" displayFormat="yyyy-MM-dd" />
                <s:submit  value="SAVE"></s:submit>
            </s:form>
        </div>
    </body>
</html>
