package com.finextreme.loans;
/**
 *
 * @author vinn
 */
import com.opensymphony.xwork2.ActionSupport;

public class LoanType extends ActionSupport {
		
	private static final long serialVersionUID = 1L;
	private String loanTypeCode;
	private String loanShortName; 
	private String loanSerialIdentifier;
	private String loanTypeName;     
	private Double percentage; 
	private String loanDual; 
	private String loanFlatRate; 
	private String loanReducingBalance; 
	private String loanMaxDuration; 
	private String loanNumOfTimesShares; 
	private String loanMaxAmount; 
	private String loanRoundOff; 
	private String loanGracePeriod; 
	private String waitingPeriod; 
	private String loanAccount;    
	private Double commission; 
	private String interestAccount; 
	private int repayFormular; 
	private int loanUsed; 
	private String schemeUsed; 
	private Boolean usesSalary; 
	private String accHeldInTrust; 
	private String prdCd; 
	private int useCommittedShares; 
	private int clearSameCalendarYear ;
	private String classCode; 
	private String accCreditLiab; 
	private Boolean allowLnOfSameType; 
	private Boolean recomendDormant; 
	private Boolean constantInt; 
	private Boolean mFILoan; 
	private Boolean recomendDefault; 
	private Double maxLoanType; 
	private Double salaryPerc; 
	private Double minInterest; 
	private Boolean usesDividends ;
	private Double dividendPerc; 
	private Boolean noGuarantorsRequired; 
	private Boolean noDeductionRequired; 
	private Boolean productClosed; 
	private Boolean ignoreSalComputationIfBlank; 
	private Boolean isFosaLoan; 
	private Boolean isFosaAdvance; 
	private Boolean isIntrCatDefined; 
	private Boolean requiresSalary; 
	private Boolean recFromSal; 
	private Boolean recFromCashDep; 
	private Boolean recFromChqs; 
	private Boolean recFromRefunds;
	private Boolean recoverFromLoans; 
	private Boolean recAuto; 
	private Boolean recoverFromDividend; 
	private Boolean applyAffidavit; 
	private Boolean mustHaveSharesRegardless; 
	private Boolean fOSASchemeUsed; 
	private String fOSAScheme; 
	private String fOSASchemeClassCode;
	private int takeGuarantorsAS ;
	private Double maximumNumberOfGuarantors ;
	private Double minimumNumberOfGuarantors ;
	private Boolean splitFOSARepayments; 
	private int instantIntLastDate; 
	private Boolean noMonthlyInt ;
	private Boolean recPrincAsPT; 
	private Boolean chargeIntImmediately; 
	private Boolean allowTopUp; 
	private Boolean isRefinanceLoan; 
	private Boolean recDividends; 
	private Boolean debitLoanWithTotalInterestOnPosting; 
	
	/*public String execute() throws Exception {
	    return "success";
	}*/
	public String execute(){  
	 int i = CreateLoanType.save(this);  
	 if(i>0){  
		 return "success";  
      }  
	    return "error";  
    }   
	public String getLoanTypeCode() {
	   return loanTypeCode;
	}
	public void setLoanTypeCode(String loanTypeCode) {
	   this.loanTypeCode = loanTypeCode;
	}
	public String getLoanShortName() {
		return loanShortName;
	}	
	public void setLoanShortName(String loanShortName) {
		this.loanShortName = loanShortName;
	}
	public String getLoanSerialIdentifier() {
	   return loanSerialIdentifier;
	}
	public void setLoanSerialIdentifier(String loanSerialIdentifier) {
	   this.loanSerialIdentifier = loanSerialIdentifier;
	}
	public String getLoanTypeName() {
		return loanTypeName;
	}
	public void setLoanTypeName(String loanTypeName) {
	   this.loanTypeName = loanTypeName;
	}
	public Double getPercentage() {
		return percentage;
	}
	public void setPercentage(Double percentage) {
	   this.percentage = percentage;
	}
	public String getLoanDual() {
		return loanDual;
	}
	public void setPercentage(String loanDual) {
	   this.loanDual = loanDual;
	}
	public String getLoanFlatRate() {
		return loanFlatRate;
	}
	public void setLoanFlatRate(String loanFlatRate) {
	   this.loanFlatRate = loanFlatRate;
	}
	public String getLoanReducingBalance() {
		return loanReducingBalance;
	}
	public void setLoanReducingBalance(String loanReducingBalance) {
	   this.loanReducingBalance = loanReducingBalance;
	}
	public String getLoanMaxDuration() {
		return loanMaxDuration;
	}
	public void setLoanMaxDuration(String loanMaxDuration) {
	   this.loanMaxDuration = loanMaxDuration;
	}
	public Boolean getUsesSalary() {
		return usesSalary;
	}
	public void setUsesSalary(Boolean usesSalary) {
	   this.usesSalary = usesSalary;
	}
	public String getLoanNumOfTimesShares() {
		return loanNumOfTimesShares;
	}
	public void setLoanNumOfTimesShares(String loanNumOfTimesShares) {
	   this.loanNumOfTimesShares = loanNumOfTimesShares;
	}
	public String getLoanGracePeriod() {
		return loanGracePeriod;
	}
	public void setLoanGracePeriod(String loanGracePeriod) {
	   this.loanGracePeriod = loanGracePeriod;
	}
	public String getLoanMaxAmount() {
		return loanMaxAmount;
	}
	public void setLoanMaxAmount(String loanMaxAmount) {
	   this.loanMaxAmount = loanMaxAmount;
	}
	public String getLoanRoundOff() {
		return loanRoundOff;
	}
	public void setLoanRoundOff(String loanRoundOff) {
	   this.loanRoundOff = loanRoundOff;
	}
	public String getWaitingPeriod() {
		return waitingPeriod;
	}
	public void setWaitingPeriod(String waitingPeriod) {
	   this.waitingPeriod = waitingPeriod;
	}
	public String getLoanAccount() {
		return loanAccount;
	}
	public void setLoanAccount(String loanAccount) {
	   this.loanAccount = loanAccount;
	}
	public Double getCommission() {
		return commission;
	}
	public void setCommission(Double commission) {
	   this.commission = commission;
	}
	public String getInterestAccount() {
		return interestAccount;
	}
	public void setInterestAccount(String interestAccount) {
	   this.interestAccount = interestAccount;
	}
	public int getRepayFormular() {
		return repayFormular;
	}
	public void setRepayFormular(int repayFormular) {
	   this.repayFormular = repayFormular;
	}
	public int getLoanUsed() {
		return loanUsed;
	}
	public void setLoanUsed(int loanUsed) {
	   this.loanUsed = loanUsed;
	}
	public String getSchemeUsed() {
		return schemeUsed;
	}
	public void setSchemeUsed(String schemeUsed) {
	   this.schemeUsed = schemeUsed;
	}
	public String getAccHeldInTrust() {
		return accHeldInTrust;
	}
	public void setAccHeldInTrust(String accHeldInTrust) {
	   this.accHeldInTrust = accHeldInTrust;
	}
	public String getClassCode() {
		return classCode;
	}
	public void setClassCode(String classCode) {
	   this.classCode = classCode;
	}
	public String getPrdCd() {
		return prdCd;
	}
	public void setPrdCd(String prdCd) {
	   this.prdCd = prdCd;
	}
	public int getUseCommittedShares() {
		return useCommittedShares;
	}
	public void setUseCommittedShares(int useCommittedShares) {
	   this.useCommittedShares = useCommittedShares;
	}
	public int getClearSameCalendarYear() {
		return clearSameCalendarYear;
	}
	public void setClearSameCalendarYear(int clearSameCalendarYear) {
	   this.clearSameCalendarYear = clearSameCalendarYear;
	}
	public String getAccCreditLiab() {
		return accCreditLiab;
	}
	public void setAccCreditLiab(String accCreditLiab) {
	   this.accCreditLiab = accCreditLiab;
	}
	public Boolean getAllowLnOfSameType() {
		return allowLnOfSameType;
	}
	public void setAllowLnOfSameType(Boolean allowLnOfSameType) {
	   this.allowLnOfSameType = allowLnOfSameType;
	}
	public Boolean getRecomendDormant() {
		return recomendDormant;
	}
	public void setRecomendDormant(Boolean recomendDormant) {
	   this.recomendDormant = recomendDormant;
	}
	public Boolean getConstantInt() {
		return constantInt;
	}
	public void setConstantInt(Boolean constantInt) {
	   this.constantInt = constantInt;
	}
	public Boolean getMFILoan() {
		return mFILoan;
	}
	public void setMFILoan(Boolean mFILoan) {
	   this.mFILoan = mFILoan;
	}
	public Boolean getRecomendDefault() {
		return recomendDefault;
	}
	public void setRecomendDefault(Boolean recomendDefault) {
	   this.recomendDefault = recomendDefault;
	}
	public Double getMaxLoanType() {
		return maxLoanType;
	}
	public void setMaxLoanType(Double maxLoanType) {
	   this.maxLoanType = maxLoanType;
	}
	public Double getSalaryPerc() {
		return salaryPerc;
	}
	public void setSalaryPerc(Double salaryPerc) {
	   this.salaryPerc = salaryPerc;
	}
	public Double getMinInterest() {
		return minInterest;
	}
	public void setMinInterest(Double minInterest) {
	   this.minInterest = minInterest;
	}
	public Boolean getUsesDividends() {
		return usesDividends;
	}
	public void setUsesDividends(Boolean usesDividends) {
	   this.usesDividends = usesDividends;
	}
	public Double getDividendPerc() {
		return dividendPerc;
	}
	public void setDividendPerc(Double dividendPerc) {
	   this.dividendPerc = dividendPerc;
	}
	public Boolean getNoGuarantorsRequired() {
		return noGuarantorsRequired;
	}
	public void setNoGuarantorsRequired(Boolean noGuarantorsRequired) {
	   this.noGuarantorsRequired = noGuarantorsRequired;
	}
	public Boolean getNoDeductionRequired() {
		return noDeductionRequired;
	}
	public void setNoDeductionRequired(Boolean noDeductionRequired) {
	   this.noDeductionRequired = noDeductionRequired;
	}
	public Boolean getProductClosed() {
		return productClosed;
	}
	public void setProductClosed(Boolean productClosed) {
	   this.productClosed = productClosed;
	}
	public Boolean getIgnoreSalComputationIfBlank() {
		return ignoreSalComputationIfBlank;
	}
	public void setIgnoreSalComputationIfBlank(Boolean ignoreSalComputationIfBlank) {
	   this.ignoreSalComputationIfBlank = ignoreSalComputationIfBlank;
	}
	public Boolean getIsFosaLoan() {
		return isFosaLoan;
	}
	public void setIsFosaLoan(Boolean isFosaLoan) {
	   this.isFosaLoan = isFosaLoan;
	}
	public Boolean getIsFosaAdvance() {
		return isFosaAdvance;
	}
	public void setIsFosaAdvance(Boolean isFosaAdvance) {
	   this.isFosaAdvance = isFosaAdvance;
	}
	public Boolean getRequiresSalary() {
		return requiresSalary;
	}
	public void setRequiresSalary(Boolean requiresSalary) {
	   this.requiresSalary = requiresSalary;
	}
	public Boolean getIsIntrCatDefined() {
		return isIntrCatDefined;
	}
	public void setIsIntrCatDefined(Boolean isIntrCatDefined) {
	   this.isIntrCatDefined = isIntrCatDefined;
	}
	public Boolean getRecFromSal() {
		return recFromSal;
	}
	public void setRecFromSal(Boolean recFromSal) {
	   this.recFromSal = recFromSal;
	}
	public Boolean getRecFromCashDep() {
		return recFromCashDep;
	}
	public void setRecFromCashDep(Boolean recFromCashDep) {
	   this.recFromCashDep = recFromCashDep;
	}
	public Boolean getRecFromChqs() {
		return recFromChqs;
	}
	public void setRecFromChqs(Boolean recFromChqs) {
	   this.recFromChqs = recFromChqs;
	}
	public Boolean getRecFromRefunds() {
		return recFromRefunds;
	}
	public void setRecFromRefunds(Boolean recFromRefunds) {
	   this.recFromRefunds = recFromRefunds;
	}
	public Boolean getRecoverFromLoans() {
		return recoverFromLoans;
	}
	public void setRecoverFromLoans(Boolean recoverFromLoans) {
	   this.recoverFromLoans = recoverFromLoans;
	}
	public Boolean getRecAuto() {
		return recAuto;
	}
	public void setRecAuto(Boolean recAuto) {
	   this.recAuto = recAuto;
	}
	public Boolean getRecoverFromDividend() {
		return recoverFromDividend;
	}
	public void setRecoverFromDividend(Boolean recoverFromDividend) {
	   this.recoverFromDividend = recoverFromDividend;
	}
	public Boolean getApplyAffidavit() {
		return applyAffidavit;
	}
	public void setApplyAffidavit(Boolean applyAffidavit) {
	   this.applyAffidavit = applyAffidavit;
	}
	public Boolean getMustHaveSharesRegardless() {
		return mustHaveSharesRegardless;
	}
	public void setMustHaveSharesRegardless(Boolean mustHaveSharesRegardless) {
	   this.mustHaveSharesRegardless = mustHaveSharesRegardless;
	}
	public Boolean getFOSASchemeUsed() {
		return fOSASchemeUsed;
	}
	public void setFOSASchemeUsed(Boolean fOSASchemeUsed) {
	   this.fOSASchemeUsed = fOSASchemeUsed;
	}
	public String getFOSAScheme() {
		return fOSAScheme;
	}
	public void setFOSAScheme(String fOSAScheme) {
	   this.fOSAScheme = fOSAScheme;
	}
	public String getFOSASchemeClassCode() {
		return fOSASchemeClassCode;
	}
	public void setFOSASchemeClassCode(String fOSASchemeClassCode) {
	   this.fOSASchemeClassCode = fOSASchemeClassCode;
	}
	public int getTakeGuarantorsAS() {
		return takeGuarantorsAS;
	}
	public void setTakeGuarantorsAS(int takeGuarantorsAS) {
	   this.takeGuarantorsAS = takeGuarantorsAS;
	}
	public Double getMaximumNumberOfGuarantors() {
		return maximumNumberOfGuarantors;
	}
	public void setMaximumNumberOfGuarantors(Double maximumNumberOfGuarantors) {
	   this.maximumNumberOfGuarantors = maximumNumberOfGuarantors;
	}
	public Double getMinimumNumberOfGuarantors() {
		return minimumNumberOfGuarantors;
	}
	public void setMinimumNumberOfGuarantors(Double minimumNumberOfGuarantors) {
	   this.minimumNumberOfGuarantors = minimumNumberOfGuarantors;
	}
	public Boolean getSplitFOSARepayments() {
		return splitFOSARepayments;
	}
	public void setSplitFOSARepayments(Boolean splitFOSARepayments) {
	   this.splitFOSARepayments = splitFOSARepayments;
	}
	public int getInstantIntLastDate() {
		return instantIntLastDate;
	}
	public void setInstantIntLastDate(int instantIntLastDate) {
	   this.instantIntLastDate = instantIntLastDate;
	}
	public Boolean getNoMonthlyInt() {
		return noMonthlyInt;
	}
	public void setNoMonthlyInt(Boolean noMonthlyInt) {
	   this.noMonthlyInt = noMonthlyInt;
	}
	public Boolean getRecPrincAsPT() {
		return recPrincAsPT;
	}
	public void setRecPrincAsPT(Boolean recPrincAsPT) {
	   this.recPrincAsPT = recPrincAsPT;
	}
	public Boolean getChargeIntImmediately() {
		return chargeIntImmediately;
	}
	public void setChargeIntImmediately(Boolean chargeIntImmediately) {
	   this.chargeIntImmediately = chargeIntImmediately;
	}
	public Boolean getAllowTopUp() {
		return allowTopUp;
	}
	public void setAllowTopUp(Boolean allowTopUp) {
	   this.allowTopUp = allowTopUp;
	}
	public Boolean getIsRefinanceLoan() {
		return isRefinanceLoan;
	}
	public void setIsRefinanceLoan(Boolean isRefinanceLoan) {
	   this.isRefinanceLoan = isRefinanceLoan;
	}
	public Boolean getRecDividends() {
		return recDividends;
	}
	public void setRecDividends(Boolean recDividends) {
	   this.recDividends = recDividends;
	}
	public Boolean getDebitLoanWithTotalInterestOnPosting() {
		return debitLoanWithTotalInterestOnPosting;
	}
	public void setDebitLoanWithTotalInterestOnPosting(Boolean debitLoanWithTotalInterestOnPosting) {
	   this.debitLoanWithTotalInterestOnPosting = debitLoanWithTotalInterestOnPosting;
	}	

}
