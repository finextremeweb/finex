package com.finextreme.loans;

/**
 *
 * @author vinn
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.interceptor.ServletRequestAware;
import com.opensymphony.xwork2.ActionSupport;

public class ViewLoanType extends ActionSupport implements ServletRequestAware{
	private static final long serialVersionUID = 1L;

	HttpServletRequest request;

	public String execute()
	{
		
		try{
			Connection con = null;
			String URL = "jdbc:mysql://127.0.0.1:3306/main";  
                        Class.forName("com.mysql.jdbc.Driver");  
                        con = DriverManager.getConnection(URL, "root", "kipvenx");
			Statement st=con.createStatement();
			ResultSet rs = st.executeQuery("select * from loantypes");
	
                        List li = null;
                        li = new ArrayList();
                        Mybean mb = null;
	
			while(rs.next())
			{
				    mb = new Mybean();
	
				    mb.setNo(rs.getInt("LoanTypeCode"));
				    mb.setNam(rs.getString("LoanShortName"));
				    mb.setCt(rs.getString("LoanTypeName"));		  
	
				    li.add(mb);
	
			}
	
			request.setAttribute("disp", li);
	
			rs.close();
	 		st.close();
			con.close();
	
		}
		catch(Exception e){
	 		e.printStackTrace();
	 	}
	
		return SUCCESS;
	
	}
	
	public void setServletRequest(HttpServletRequest request) {
	        this.request = request;
	}

    public HttpServletRequest getServletRequest() {
        return request;
}

}