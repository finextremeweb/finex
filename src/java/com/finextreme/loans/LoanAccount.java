package com.finextreme.loans;

/**
 *
 * @author vinn
 */
import com.opensymphony.xwork2.ActionSupport;

public class LoanAccount extends ActionSupport {
		
	private static final long serialVersionUID = 1L;
	
	public enum choice {
	    YES, Y, NO, N
	}
	
	private String LoanAcc;
	private Double Interest;
	private String InterestAcc;
	private int RepaymentPeriod;
	private choice IssueWithPreviousLoan;
	private choice RecoverFromCheques;
	private choice RecoverFromSalary;
	private choice RecoverFromLoans;
	private choice RecoverFromCashDeposits;
	private choice RecoverFromDividend;
	private Double PenaltyPercOnChequeBounce;
	private choice RecoverFromBosaPayments;
	private choice AutoRecover;
	private String PenaltyAcc;
	private Double FailSavingsBelowAmount;
	private int PercOfNetSal;
	private Double MaxLoanAmt;
	private choice IntMode;
	private Double PenyAmt;
	private Double PenMinAmount;
	private choice PenRateMode;
	private String PenAccount;
	private choice MicroFinanceLoan;
	
		
	/*public String execute() throws Exception {
	    return "success";
	}*/
	public String execute(){  
            int i = 0; // SaveLoanAccount.save(this);  
            if(i>0){  
                    return "success";  
            }  
	    return "error";  
        }   
	public String getLoanAcc() {
	   return LoanAcc;
	}
	public void setLoanAcc(String LoanAcc) {
	   this.LoanAcc = LoanAcc;
	}
	public Double getInterest() {
	   return Interest;
	}
	public void setInterest(Double Interest) {
	   this.Interest = Interest;
	}
	public String getInterestAcc() {
		return InterestAcc;
	}
	public void setInterestAcc(String InterestAcc) {
		this.InterestAcc = InterestAcc;
	}
	public int getRepaymentPeriod() {
		return RepaymentPeriod;
	}
	public void setRepaymentPeriod(int RepaymentPeriod) {
		this.RepaymentPeriod = RepaymentPeriod;
	}
	public choice getIssueWithPreviousLoan() {
		return IssueWithPreviousLoan;
	}
	public void setIssueWithPreviousLoan(choice IssueWithPreviousLoan) {
		this.IssueWithPreviousLoan = IssueWithPreviousLoan;
	}
	public choice getRecoverFromCheques() {
		return RecoverFromCheques;
	}
	public void setRecoverFromCheques(choice RecoverFromCheques) {
		this.RecoverFromCheques = RecoverFromCheques;
	}
	public choice getRecoverFromSalary() {
		return RecoverFromSalary;
	}
	public void setRecoverFromSalary(choice RecoverFromSalary) {
		this.RecoverFromSalary = RecoverFromSalary;
	}
	public choice getRecoverFromLoans() {
		return RecoverFromLoans; 
	}
	public void setRecoverFromLoans(choice RecoverFromLoans) {
		this.RecoverFromLoans = RecoverFromLoans;
	}
	public choice getRecoverFromCashDeposits() {
		return RecoverFromCashDeposits; 
	}
	public void setRecoverFromCashDeposits(choice RecoverFromCashDeposits) {
		this.RecoverFromCashDeposits = RecoverFromCashDeposits;
	}
	public choice getRecoverFromDividend() {
		return RecoverFromDividend; 
	}
	public void setRecoverFromDividend(choice RecoverFromDividend) {
		this.RecoverFromDividend = RecoverFromDividend;
	}
	public Double getPenaltyPercOnChequeBounce() {
		return PenaltyPercOnChequeBounce; 
	}
	public void setPenaltyPercOnChequeBounce(Double PenaltyPercOnChequeBounce) {
		this.PenaltyPercOnChequeBounce = PenaltyPercOnChequeBounce;
	}
	public choice getRecoverFromBosaPayments() {
		return RecoverFromBosaPayments; 
	}
	public void setRecoverFromBosaPayments(choice RecoverFromBosaPayments) {
		this.RecoverFromBosaPayments = RecoverFromBosaPayments;
	}
	
	/*}
	public void setPercentage(String loanDual) {
	   this.loanDual = loanDual;
	}
	public String getLoanFlatRate() {
		return loanFlatRate;
	}
	public void setLoanFlatRate(String loanFlatRate) {
	   this.loanFlatRate = loanFlatRate;
	}
	public String getLoanReducingBalance() {
		return loanReducingBalance;
	}
	public void setLoanReducingBalance(String loanReducingBalance) {
	   this.loanReducingBalance = loanReducingBalance;
	}
	public String getLoanMaxDuration() {
		return loanMaxDuration;
	}
	public void setLoanMaxDuration(String loanMaxDuration) {
	   this.loanMaxDuration = loanMaxDuration;
	}
	public Boolean getUsesSalary() {
		return usesSalary;
	}
	public void setUsesSalary(Boolean usesSalary) {
	   this.usesSalary = usesSalary;
	}
	public String getLoanNumOfTimesShares() {
		return loanNumOfTimesShares;
	}
	public void setLoanNumOfTimesShares(String loanNumOfTimesShares) {
	   this.loanNumOfTimesShares = loanNumOfTimesShares;
	}
	public String getLoanGracePeriod() {
		return loanGracePeriod;
	}
	public void setLoanGracePeriod(String loanGracePeriod) {
	   this.loanGracePeriod = loanGracePeriod;
	}
	public String getLoanMaxAmount() {
		return loanMaxAmount;
	}
	public void setLoanMaxAmount(String loanMaxAmount) {
	   this.loanMaxAmount = loanMaxAmount;
	}
	public String getLoanRoundOff() {
		return loanRoundOff;
	}
	public void setLoanRoundOff(String loanRoundOff) {
	   this.loanRoundOff = loanRoundOff;
	}
	public String getWaitingPeriod() {
		return waitingPeriod;
	}
	public void setWaitingPeriod(String waitingPeriod) {
	   this.waitingPeriod = waitingPeriod;
	}
	public String getLoanAccount() {
		return loanAccount;
	}
	public void setLoanAccount(String loanAccount) {
	   this.loanAccount = loanAccount;
	}
	public Double getCommission() {
		return commission;
	}
	public void setCommission(Double commission) {
	   this.commission = commission;
	}
	public String getInterestAccount() {
		return interestAccount;
	}
	public void setInterestAccount(String interestAccount) {
	   this.interestAccount = interestAccount;
	}
	public int getRepayFormular() {
		return repayFormular;
	}
	public void setRepayFormular(int repayFormular) {
	   this.repayFormular = repayFormular;
	}
	public int getLoanUsed() {
		return loanUsed;
	}
	public void setLoanUsed(int loanUsed) {
	   this.loanUsed = loanUsed;
	}
	public String getSchemeUsed() {
		return schemeUsed;
	}
	public void setSchemeUsed(String schemeUsed) {
	   this.schemeUsed = schemeUsed;
	}
	public String getAccHeldInTrust() {
		return accHeldInTrust;
	}
	public void setAccHeldInTrust(String accHeldInTrust) {
	   this.accHeldInTrust = accHeldInTrust;
	}
	public String getClassCode() {
		return classCode;
	}
	public void setClassCode(String classCode) {
	   this.classCode = classCode;
	}
	public String getPrdCd() {
		return prdCd;
	}
	public void setPrdCd(String prdCd) {
	   this.prdCd = prdCd;
	}
	public int getUseCommittedShares() {
		return useCommittedShares;
	}
	public void setUseCommittedShares(int useCommittedShares) {
	   this.useCommittedShares = useCommittedShares;
	}
	public int getClearSameCalendarYear() {
		return clearSameCalendarYear;
	}
	public void setClearSameCalendarYear(int clearSameCalendarYear) {
	   this.clearSameCalendarYear = clearSameCalendarYear;
	}
	public String getAccCreditLiab() {
		return accCreditLiab;
	}
	public void setAccCreditLiab(String accCreditLiab) {
	   this.accCreditLiab = accCreditLiab;
	}
	public Boolean getAllowLnOfSameType() {
	 setMaxLoanType(Double maxLoanType) {
	   this.maxLoanType = maxLoanType;
	}
	public Double getSalaryPerc() {
		return salaryPerc;
	}
	public void setNoDeductionRequired(Boolean noDeductionRequired) {
	   this.noDeductionRequired = noDeductionRequired;
	}
	public Boolean getProductClosed() {
		return productClosed;
	}
	public void setProductClosed(Boolean productClosed) {
	   this.productClosed = productClosed;
	}
	public Boolean getIgnoreSalComputationIfBlank() {
		return ignoreSalComputationIfBlank;
	}
	public void setIgnoreSalComputationIfBlank(Boolean ignoreSalComputationIfBlank) {
	   this.ignoreSalComputationIfBlank = ignoreSalComputationIfBlank;
	}
	public Boolean getIsFosaLoan() {
		return isFosaLoan;
	}
	public void setIsFosaLoan(Boolean isFosaLoan) {
	   this.isFosaLoan = isFosaLoan;
	}
	public Boolean getIsFosaAdvance() {
		return isFosaAdvance;
	}
	public void setIsFosaAdvance(Boolean isFosaAdvance) {
	   this.isFosaAdvance = isFosaAdvance;
	}
	public Boolean getRequiresSalary() {
		return requiresSalary;
	}
	public void setRequiresSalary(Boolean requiresSalary) {
	   this.requiresSalary = requiresSalary;
	}
	public Boolean getIsIntrCatDefined() {
		return isIntrCatDefined;
	}
	public void setIsIntrCatDefined(Boolean isIntrCatDefined) {
	   this.isIntrCatDefined = isIntrCatDefined;
	}
	public Boolean getRecFromSal() {
		return recFromSal;
	}
	public void setRecFromSal(Boolean recFromSal) {
	   this.recFromSal = recFromSal;
	}
	public Boolean getRecFromCashDep() {
		return recFromCashDep;
	}
	public void setRecFromCashDep(Boolean recFromCashDep) {
	   this.recFromCashDep = recFromCashDep;
	}
	public Boolean getRecFromChqs() {
		return recFromChqs;
	}
	public void setRecFromChqs(Boolean recFromChqs) {
	   this.recFromChqs = recFromChqs;
	}
	public Boolean getRecFromRefunds() {
		return recFromRefunds;
	}
	public void setRecFromRefunds(Boolean recFromRefunds) {
	   this.recFromRefunds = recFromRefunds;
	}
	public Boolean getRecoverFromLoans() {
		return recoverFromLoans;
	}
	public void setRecoverFromLoans(Boolean recoverFromLoans) {
	   this.recoverFromLoans = recoverFromLoans;
	}
	public Boolean getRecAuto() {
		return recAuto;
	}
	public void setRecAuto(Boolean recAuto) {
	   this.recAuto = recAuto;
	}
	public Boolean getRecoverFromDividend() {
		return recoverFromDividend;
	}
	public void setRecoverFromDividend(Boolean recoverFromDividend) {
	   this.recoverFromDividend = recoverFromDividend;
	}
	public Boolean getApplyAffidavit() {
		return applyAffidavit;
	}
	public void setApplyAffidavit(Boolean applyAffidavit) {
	   this.applyAffidavit = applyAffidavit;
	}
	public Boolean getMustHaveSharesRegardless() {
		return mustHaveSharesRegardless;
	}
	public void setMustHaveSharesRegardless(Boolean mustHaveSharesRegardless) {
	   this.mustHaveSharesRegardless = mustHaveSharesRegardless;
	}
	public Boolean getFOSASchemeUsed() {
		return fOSASchemeUsed;
	}
	public void setFOSASchemeUsed(Boolean fOSASchemeUsed) {
	   this.fOSASchemeUsed = fOSASchemeUsed;
	}
	public String getFOSAScheme() {
		return fOSAScheme;
	}
	public void setFOSAScheme(String fOSAScheme) {
	   this.fOSAScheme = fOSAScheme;
	}
	public String getFOSASchemeClassCode() {
		return fOSASchemeClassCode;
	}
	public void setFOSASchemeClassCode(String fOSASchemeClassCode) {
	   this.fOSASchemeClassCode = fOSASchemeClassCode;
	}
	public int getTakeGuarantorsAS() {
		return takeGuarantorsAS;
	}
	public void setTakeGuarantorsAS(int takeGuarantorsAS) {
	   this.takeGuarantorsAS = takeGuarantorsAS;
	}
	public Double getMaximumNumberOfGuarantors() {
		return maximumNumberOfGuarantors;
	}
	public void setMaximumNumberOfGuarantors(Double maximumNumberOfGuarantors) {
	   this.maximumNumberOfGuarantors = maximumNumberOfGuarantors;
	}
	public Double getMinimumNumberOfGuarantors() {
		return minimumNumberOfGuarantors;
	}
	public void setMinimumNumberOfGuarantors(Double minimumNumberOfGuarantors) {
	   this.minimumNumberOfGuarantors = minimumNumberOfGuarantors;
	}
	public Boolean getSplitFOSARepayments() {
		return splitFOSARepayments;
	}
	public void setSplitFOSARepayments(Boolean splitFOSARepayments) {
	   this.splitFOSARepayments = splitFOSARepayments;
	}
	public int getInstantIntLastDate() {
		return instantIntLastDate;
	}
	public void setInstantIntLastDate(int instantIntLastDate) {
	   this.instantIntLastDate = instantIntLastDate;
	}
	public Boolean getNoMonthlyInt() {
		return noMonthlyInt;
	}
	public void setNoMonthlyInt(Boolean noMonthlyInt) {
	   this.noMonthlyInt = noMonthlyInt;
	}
	public Boolean getRecPrincAsPT() {
		return recPrincAsPT;
	}
	public void setRecPrincAsPT(Boolean recPrincAsPT) {
	   this.recPrincAsPT = recPrincAsPT;
	} 

	public Boolean getChargeIntImmediately() {
		return chargeIntImmediately;
	}
	public void setChargeIntImmediately(Boolean chargeIntImmediately) {
	   this.chargeIntImmediately = chargeIntImmediately;
	}
	public Boolean getAllowTopUp() {
		return allowTopUp;
	}
	public void setAllowTopUp(Boolean allowTopUp) {
	   this.allowTopUp = allowTopUp;
	}
	public Boolean getIsRefinanceLoan() {
		return isRefinanceLoan;
	}
	public void setIsRefinanceLoan(Boolean isRefinanceLoan) {
	   this.isRefinanceLoan = isRefinanceLoan;
	}
	public Boolean getRecDividends() {
		return recDividends;
	}
	public void setRecDividends(Boolean recDividends) {
	   this.recDividends = recDividends;
	}
	public Boolean getDebitLoanWithTotalInterestOnPosting() {
		return debitLoanWithTotalInterestOnPosting;
	}
	public void setDebitLoanWithTotalInterestOnPosting(Boolean debitLoanWithTotalInterestOnPosting) {
	   this.debitLoanWithTotalInterestOnPosting = debitLoanWithTotalInterestOnPosting;
	}	*/

}

