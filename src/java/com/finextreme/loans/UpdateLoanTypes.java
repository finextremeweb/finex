package com.finextreme.loans;

/**
 *
 * @author vinn
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.opensymphony.xwork2.ActionSupport;

public class UpdateLoanTypes extends ActionSupport{
	private static final long serialVersionUID = 1L;
	 
	Mybean mb=new Mybean();	
	 
	public Mybean getMb() {
		return mb;
	}
	public void setMb(Mybean mb) {
		this.mb = mb;
	}
	 
	public String execute()
	{
	 
		try{
			Connection con = null;
			String URL = "jdbc:mysql://127.0.0.1:3306/main";  
	        Class.forName("com.mysql.jdbc.Driver");  
	        con = DriverManager.getConnection(URL, "root", "kipvenx");			 
			String s = "update loantypes set LoanShortName=?,LoanTypeName=? where LoanTypeCode=?";
			PreparedStatement ps=con.prepareStatement(s);
			ps.setString(1, mb.getNam());
			ps.setString(2, mb.getCt());
			ps.setInt(3, mb.getNo());
			 
			ps.executeUpdate();
			con.commit();
			 
			ps.close();
	    
		 }
		catch(Exception e){
			e.printStackTrace();
		}
		 
		return SUCCESS;
		 
		}
 
}