package com.finextreme.loans;

/** Leo
 *
 * @author vinn
 */
import java.sql.*;

public class CreateLoanType {

public static int save(LoanType lt){  
    
    int status=0;  
    Connection con = null;
    
        try{  

            String URL = "jdbc:mysql://127.0.0.1:3306/main";  
            Class.forName("com.mysql.jdbc.Driver");  
            con = DriverManager.getConnection(URL, "root", "kipvenx");

            PreparedStatement ps=con.prepareStatement("insert into loantypes "
                + "(LoanTypeCode,LoanShortName,LoanTypeName,Percentage,"
                + "LoanDual,LoanFlatRate,LoanReducingBalance,LoanMaxDuration,LoanNumOfTimesShares,"
                + "LoanMaxAmount,LoanRoundOff,LoanGracePeriod,WaitingPeriod,LoanAccount,"
                + "Commission,InterestAccount,RepayFormular,LoanUsed,UsesSalary,"
                + "UseCommittedShares,"
                + "AllowLnOfSameType,RecomendDormant,ConstantInt,MFILoan,"
                + "RecomendDefault,MaxLoanType,SalaryPerc,MinInterest,UsesDividends,"
                + "DividendPerc,NoGuarantorsRequired,NoDeductionRequired,ProductClosed,"
                + "IgnoreSalComputationIfBlank,IsFosaLoan,IsFosaAdvance,IsIntrCatDefined) "
                + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
                + "?,?,?,?,?,?)");  					

                //?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?
            double tester = 9.0;
            int grace = 1;
            ps.setString(1,lt.getLoanTypeCode());  
            ps.setString(2,lt.getLoanShortName());  
            //ps.setString(3,lt.getLoanSerialIdentifier());  //LoanSerialIdentifier
            ps.setString(3,lt.getLoanTypeName());  
            ps.setDouble(4,tester);
            //ps.setDouble(5,lt.getPercentage());			
            ps.setString(5,lt.getLoanDual());
            ps.setString(6,lt.getLoanFlatRate());
            ps.setString(7,lt.getLoanReducingBalance());
            ps.setString(8,lt.getLoanMaxDuration());			
            ps.setString(9,lt.getLoanNumOfTimesShares());
            ps.setString(10,lt.getLoanMaxAmount());
            ps.setString(11,lt.getLoanRoundOff());
            //ps.setString(13,lt.getLoanGracePeriod());
            ps.setInt(12,grace);
            ps.setString(13,lt.getWaitingPeriod());				
            ps.setString(14,lt.getLoanAccount());			
            ps.setDouble(15,lt.getCommission());
            ps.setString(16,lt.getInterestAccount());
            ps.setInt(17,lt.getRepayFormular());
            ps.setInt(18,lt.getLoanUsed());
            //ps.setString(20,lt.getSchemeUsed());	//SchemeUsed						
            if (lt.getUsesSalary() == true){
                    ps.setBoolean(19,lt.getUsesSalary());	
            }
            else{
                    ps.setBoolean(19,lt.getUsesSalary());	
            }
            //ps.setString(22,lt.getAccHeldInTrust()); //AccHeldInTrust
            //ps.setString(23,lt.getPrdCd()); //PrdCd
            ps.setInt(20,lt.getUseCommittedShares());
            //ps.setInt(21,lt.getClearSameCalendarYear()); //ClearSameCalendarYear
            //ps.setString(26,lt.getClassCode());	//ClassCode
            //ps.setString(27,lt.getAccCreditLiab());	//AccCreditLiab		
            ps.setBoolean(21,lt.getAllowLnOfSameType());
            ps.setBoolean(22,lt.getRecomendDormant());
            ps.setBoolean(23,lt.getConstantInt());
            ps.setBoolean(24,lt.getMFILoan());
            ps.setBoolean(25,lt.getRecomendDefault());			
            ps.setDouble(26,lt.getMaxLoanType());			
            ps.setDouble(27,lt.getSalaryPerc());
            ps.setDouble(28,lt.getMinInterest());
            ps.setBoolean(29,lt.getUsesDividends());
            ps.setDouble(30,lt.getDividendPerc());
            ps.setBoolean(31,lt.getNoGuarantorsRequired());				
            ps.setBoolean(32,lt.getNoDeductionRequired());			
            ps.setBoolean(33,lt.getProductClosed());
            ps.setBoolean(34,lt.getIgnoreSalComputationIfBlank());
            ps.setBoolean(35,lt.getIsFosaLoan());
            ps.setBoolean(36,lt.getIsFosaAdvance());
            ps.setBoolean(37,lt.getIsIntrCatDefined());				
            //ps.setBoolean(45,lt.getRequiresSalary());			
            //ps.setBoolean(46,lt.getRecFromSal());
            /*
            ps.setBoolean(47,lt.getRecFromChqs());
            ps.setBoolean(48,lt.getRecFromRefunds());
            ps.setBoolean(49,lt.getRecoverFromLoans());
            ps.setBoolean(50,lt.getRecAuto());				
            ps.setBoolean(51,lt.getRecoverFromDividend());			
            ps.setBoolean(52,lt.getApplyAffidavit());
            ps.setBoolean(53,lt.getMustHaveSharesRegardless());
            ps.setBoolean(54,lt.getFOSASchemeUsed());
            ps.setString(55,lt.getFOSAScheme());
            ps.setString(56,lt.getFOSASchemeClassCode());			
            ps.setInt(57,lt.getTakeGuarantorsAS());			
            ps.setDouble(58,lt.getMaximumNumberOfGuarantors());
            ps.setDouble(59,lt.getMinimumNumberOfGuarantors());
            ps.setBoolean(60,lt.getSplitFOSARepayments());
            ps.setInt(61,lt.getInstantIntLastDate());
            ps.setBoolean(62,lt.getNoMonthlyInt());				
            ps.setBoolean(63,lt.getRecPrincAsPT());			
            ps.setBoolean(64,lt.getChargeIntImmediately());
            ps.setBoolean(65,lt.getAllowTopUp());
            ps.setBoolean(66,lt.getRecDividends());
            ps.setBoolean(67,lt.getDebitLoanWithTotalInterestOnPosting());				   
            */      
            System.out.print(ps);
            status=ps.executeUpdate(); 			
            con.commit();			 
            ps.close();
            con.close();			 

        }catch(Exception e){e.printStackTrace();}  
            return status;  
        }  
}
