package com.finextreme.loans;

/**
 *
 * @author vinn
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.interceptor.ServletRequestAware;
import com.opensymphony.xwork2.ActionSupport;
 
public class DeleteLoanType extends ActionSupport implements ServletRequestAware{    
    private static final long serialVersionUID = 1L;
 
    HttpServletRequest request;        
 
    public String execute()
    {            
 
	    try{
	    	Connection con = null;
			String URL = "jdbc:mysql://127.0.0.1:3306/main";  
	        Class.forName("com.mysql.jdbc.Driver");  
	        con = DriverManager.getConnection(URL, "root", "kipvenx");
		    PreparedStatement ps=null;
		 
		    String cv[]=request.getParameterValues("rdel");
		 
		    for(int i=0;i<cv.length;i++)
		    {
		        ps=con.prepareStatement("delete from loantypes where LoanTypeCode=(?)");
		        int k = Integer.parseInt(cv[i]);
		        System.out.println("this is" +k);
		        ps.setInt(1,k);        
		        ps.executeUpdate();
		        con.commit();
		    }    
	 
	        ps.close();          
	        con.close();
	 
	      }
	      catch(Exception e){
	             e.printStackTrace();
	      }
	 
	      return SUCCESS;
 
    }
 
    public void setServletRequest(HttpServletRequest request) {
        this.request = request;
    }
 
    public HttpServletRequest getServletRequest() {
        return request;
    }
 
}