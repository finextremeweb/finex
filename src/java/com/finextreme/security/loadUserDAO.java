/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finextreme.security;

import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import com.finextreme.login.javaconnect;

/**
 *
 * @author Simz
 */
public class loadUserDAO extends ActionSupport {
Connection con=null;
ResultSet rs=null;
PreparedStatement pst=null;
Statement s=null;  
ArrayList<users> list = new ArrayList<users>();

    public ArrayList<users> getList() {
        return list;
    }

    public void setList(ArrayList<users> list) {
        this.list = list;
    }

    public loadUserDAO() {
    }
    
    public String execute() throws Exception {
        con = javaconnect.ConnectDB();
        try{
String sql =" select id ,username,first_name ,last_name ,email ,Type  from users order by id";

            pst = con.prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
            users user=new users();  
            
            user.setUserid(rs.getString("id"));
            user.setUsername(rs.getString("username"));
            user.setFirstname(rs.getString("first_name"));
            user.setLastname(rs.getString("last_name"));
            user.setEmailacc(rs.getString("email"));
            user.setUsertype(rs.getString("Type"));
            list.add(user);  
        }   
        }catch(Exception e){
            
        }
        return SUCCESS;
    }
    
    
}
