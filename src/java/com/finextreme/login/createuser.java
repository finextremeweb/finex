/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finextreme.login;

import com.opensymphony.xwork2.ActionSupport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author ADMIN
 */
public class createuser extends ActionSupport{
Connection con=null;
ResultSet rs=null;
PreparedStatement pst=null;
Statement s=null;
    
    
    
    private String username;
    private String firstname;
    private String lastname;
    private String email;
    private String type;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    
    public String execute(){
        con = javaconnect.ConnectDB();
        try{
            con = javaconnect.ConnectDB();
            Statement stmt;
            stmt= con.createStatement();
            String sql1="Select username from users where username= '" + getUsername() + "'";
            rs=stmt.executeQuery(sql1);
            if(rs.next()){
            JOptionPane.showMessageDialog( null, "UserName Already Exists,\nEnter Another UserName","Error", JOptionPane.ERROR_MESSAGE);
//             return ;
            }
            String sql = "insert into users(username,first_name,last_name,email,Type,password,status,state) values(?,?,?,?,?,?,?,?)";
            pst = con.prepareStatement(sql);
            pst.setString(1,getUsername());
            pst.setString(2,getFirstname());
            pst.setString(3,getLastname());
            pst.setString(4,getEmail());
            pst.setString(5, getType());
            pst.setString(6, "2015");
            pst.setString(7, "NOT ACTIVE"); 
            pst.setString(8, "NOT DORMANT");
            pst.execute();
            JOptionPane.showMessageDialog(null,"Account Created Successfully");
            return SUCCESS;
            }
            catch(Exception e){
            JOptionPane.showMessageDialog(null,e.getMessage(),"Error",1);
            addActionError(getText("Login Failed! Invalid UserName Or Password"+e.getMessage()));
            //return ERROR;
        }
//         if (getUsername().equals("admin")){
//            return SUCCESS;
//        }
//        else{
//            addActionError(getText("Login Failed! Invalid UserName Or Password"));
//            return INPUT;
//        }
  return ERROR;    
    }
    
}
