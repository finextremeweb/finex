/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finextreme.login;

import com.opensymphony.xwork2.ActionSupport;
import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author ADMIN
 */
public class LoginAction extends ActionSupport {

    Connection con = null;
    ResultSet rs = null;
    PreparedStatement pst = null;
    Statement s = null;

    private String username;
    private String password;
    public int add1;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String execute() throws Exception {
        con = javaconnect.ConnectDB();
        String query= "select * from users where username = ? and BINARY password = ? ";
            pst = con.prepareStatement(query);
            pst.setString(1,getUsername());
            pst.setString(2,getPassword());
            rs = pst.executeQuery();
            if (rs.next()){  
       
       // if (getUsername().equals("admin") && getPassword().equals("admin")) {
            //insert log details to log table
            try {
                String sql = "select max(LogId) as extend from logtable";
                pst = con.prepareStatement(sql);
                rs = pst.executeQuery();
                if (rs.next()) {
                    Integer add = rs.getInt("extend");
                    add1 = add;
                    if (add == null) {
                        add1 = 1;
                    } else {
                        add1++;
                    }
                }
                java.util.Date dt = new java.util.Date();
                java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/M/yyyy HH:mm:ss");
                String currenttime = sdf.format(dt);
                String quer = "insert into logtable (username,LogId,timelogged,datelogged,timespent)values(?,?,?,?,?)";
                pst = con.prepareStatement(quer);
                pst.setString(1, getUsername());
                pst.setInt(2, add1);
                pst.setString(3, currenttime);
                pst.setString(4, "0");
                pst.setString(5, "0");
                pst.execute();
                return SUCCESS;

            } catch (Exception e) {
            }

            
        } else {
            addActionError(getText("Login Failed! Invalid UserName Or Password"));
            return ERROR;
        }
return ERROR;
    }

    @Override
    public void validate() {
        if (getUsername().equals("")) {
            addActionError(getText("Username Required!!!"));
        } else if (getPassword().equals("")) {
            addActionError(getText("Password Required!!!"));
        }
    }
}
