/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finextreme.propertymanager.register;

import com.opensymphony.xwork2.ActionSupport;
import static com.opensymphony.xwork2.util.LocalizedTextUtil.reset;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import java.awt.HeadlessException;
import static java.nio.file.Files.size;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import com.finextreme.login.javaconnect;
import org.apache.commons.lang3.StringUtils;
//import static org.eclipse.jdt.internal.compiler.parser.Parser.name;

/**
 *
 * @author Simz
 */
public class scheme extends ActionSupport {
    Connection con = null;
    ResultSet rs = null;
    PreparedStatement pst = null;
    Statement s = null;
    private String schemeNo,
schemeName,
county,
subCounty,
mySizes,
buyingPrice,
purchaseDate;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSchemeNo() {
        return schemeNo;
    }

    public void setSchemeNo(String schemeNo) {
        this.schemeNo = schemeNo;
    }

    public String getSchemeName() {
        return schemeName;
    }

    public void setSchemeName(String schemeName) {
        this.schemeName = schemeName;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getSubCounty() {
        return subCounty;
    }

    public void setSubCounty(String subCounty) {
        this.subCounty = subCounty;
    }

    public String getMySizes() {
        return mySizes;
    }

    public void setMySizes(String mySizes) {
        this.mySizes = mySizes;
    }

    public String getBuyingPrice() {
        return buyingPrice;
    }

    public void setBuyingPrice(String buyingPrice) {
        this.buyingPrice = buyingPrice;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }
    public scheme() {
    }
    public String  tryY(){
return SUCCESS;
}
    public String execute() throws Exception {
       try{
            con=javaconnect.ConnectDB();
            Statement stmt;
            stmt= con.createStatement();
            String sql1="Select SchemesRefNumber from Schemes where SchemesRefNumber= '" + getSchemeNo() + "'";
            rs=stmt.executeQuery(sql1);
            if(rs.next()){
                addActionError(getText( "Scheme Number Already Exists,\nEnter Another Scheme Number"));
               // schemeno.setText("");
               // schemeno.requestDefaultFocus();
               // return;
            }
            String sql= "insert into Schemes(SchemesRefNumber,SchemeName,County,SubCounty,SchemeSize,SchemePrice,Registerdate)values(?,?,?,?,?,?,?)";
            pst=con.prepareStatement(sql);
            pst.setString(1,getSchemeNo());
            pst.setString(2,getSchemeName());
            pst.setString(3,getCounty());
            pst.setString(4,getSubCounty());
            pst.setString(5,getMySizes());
            pst.setString(6,getBuyingPrice());
            pst.setString(7,getPurchaseDate());
            pst.execute();
            setMessage("Scheme Saved Successfully");  
            return SUCCESS;
            //Get_Data();
           // reset();
        }catch(HeadlessException | SQLException ex){
         setMessage("Error Saving Scheme"+ex);
         return ERROR;   
        }
    }
//     public void validate() {
//        if (getSchemeNo().equals("")) {
//            addActionError(getText("Scheme No. Required!!!"));
//        } else if (getSchemeName().equals("")) {
//            addActionError(getText("Scheme Name Required!!!"));
//        }
   // }
 
    public void validate() {
//if(getUsername().equals("") && getPassword().equals("") ){
//         addActionError(getText("Text Fields Blank!!!!"));
//         
//        }  
        if(StringUtils.isEmpty(getSchemeNo())){
            addFieldError("schemeNo", "Please Insert Scheme No");
        }
        if(StringUtils.isEmpty(getSchemeName())){
            addFieldError("schemeName", "Please Insert Scheme Name");
        }
        if(StringUtils.isEmpty(getCounty())){
            addFieldError("county", "Please Insert County");
        }
        if(StringUtils.isEmpty(getSubCounty())){
            addFieldError("subCounty", "Please Insert Sub County");
        }
        if(StringUtils.equals(getMySizes(),"-Select-")){
            addFieldError("mySizes", "Please Select Scheme Size");
        }
         if(StringUtils.isEmpty(getBuyingPrice())){
            addFieldError("buyingPrice", "Please Insert Buying Price");
        }
        if(StringUtils.isEmpty(getPurchaseDate())){
            addFieldError("purchaseDate", "Please Insert Purchase Date");
        }
        if (getMySizes().equals("-Select-")) {
            addActionError(getText("Username Required!!!"));
        }
        
    }
//public String saveScheme(){
//
//        
//   
//    }
 String message;

    private void addFieldError(String field_required) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
