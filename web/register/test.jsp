<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" %>



<script type="text/javascript">
    function submit() {
        document.forms[0].submit();
    }
</script>



<html>
    <head>
        <title>Displaying Values From Database |</title>
        <style type="text/css">
            table#viewid tr:nth-child(odd) {
                background-color: #7dc9e2;    
            }
            table#viewid tr:nth-child(even){
                background-color:   #009ae1; 
            }   
        </style>

    </head>
    <body onload="submit()">






        <h5>Displayed Successfully.</h5>
        <s:form action="show">
            <table border="0" class="view" id="viewid">
                <tr style="background:  #ffffff">
                    <th>Username</th>
                    <th>Password</th>
                </tr>

                <s:iterator value="list" status="listStatus">
                    <tr>
                        <td><s:property value="username"/></td>
                        <td><s:property value="password"/></td>
                        <td><s:property value=""/></td>
                    </tr>
                </s:iterator>
            </table>

        </body>
    </s:form>
</html>