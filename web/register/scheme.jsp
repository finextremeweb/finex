<%-- 
    Document   : scheme
    Created on : Sep 10, 2015, 9:11:39 AM
    Author     : Simz
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>

<jsp:useBean id = "connect" scope = "request"         
             class = "com.finextreme.login.javaconnect" />
<%!
    Connection con = null;
    PreparedStatement pst = null;
    ResultSet rs = null;
%>
<%@page import="java.sql.*" %>
<html>
    <head>

        <sx:head/>

        <script type="text/javascript">
            function submit() {
                document.getElementsByName("combo").submit();
            }

        </script>
    </head>
    <body>
        <s:actionerror />
        <div style=" position: relative; left: 120px; width: 100%; height: 20px; background-color: greenyellow">
            <s:property value="message"/> </div>
            <s:form action="popCombo" ></s:form>
            <fieldset>
            <s:form action="saveScheme" >
                <table border="0" cellpadding="5">

                    <tbody>
                        <tr> <td>
                        <tr>
                            <td><s:textfield name="schemeName" label="Scheme Name" style="width: 250px; height: 25px;"></s:textfield>
                                </td>
                                <td><s:textfield name="schemeNo" label="Scheme No" style="width: 250px; height: 25px;"></s:textfield></td>
                            </tr>

                            <tr>
                                <td><s:textfield name="county" label="County" style="width: 250px; height: 25px;"></s:textfield></td>
                            <td><s:textfield name="subCounty" label="Sub County" style="width: 250px; height: 25px;"></s:textfield></td>
                            </tr>

                            <tr>
                                <td>Scheme Size:</td>
                                <td><select  name="mySizes" style="width: 250px; height: 25px;"> <option value="0">-Select-</option>
                                    <% try {
                                            //System.out.println("ghfdsahjkhdsjkfds");
                                            con = connect.ConnectDB();
                                            // String stream=request.getParameter("stream");
                                            pst = con.prepareStatement("select distinct Size from Scheme_Size order by Size");;//where currentClass='"+stream+"'
                                            //System.out.println(stream);
                                            rs = pst.executeQuery();
                                            System.out.println(rs);
                                            while (rs.next()) {//System.out.println("inside while");
%>
                                    <option value="<%=rs.getString("Size")%>"> <%=rs.getString("Size")%></option>
                                    <%}
                                        } catch (Exception e) {
                                        }%>
                                </select></td>
                            <td><s:textfield name="buyingPrice" label="Buying Price" style="width: 250px; height: 25px;"></s:textfield>
                                </td>
                            </tr>

                            <tr>
                                <td><sx:datetimepicker name="purchaseDate"   label="Purchase Date" displayFormat="yyyy-MM-dd" cssStyle="width: 220px; height: 25px;"/><br /></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td><s:submit value="SAVE" style="width: 250px; height: 25px;" /> <br /></td>
                            <td><s:reset value="CLEAR" style="width: 250px; height: 25px;"/>
                            </td>
                        </tr>
                        </td>

                        </tr>
                    </tbody>
                </table>
                <%-- <s:select label="Scheme Size" 
                  headerKey="1" headerValue="--- Select ---"
                  list="sizes" 
name="mySizes" style="width: 250px; height: 25px;">  </s:select> --%>


            </s:form>
        </fieldset>
    </center>
</body>
</html>
