
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sd" uri="/struts-dojo-tags"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script type="text/javascript">
    function load() {

        document.loadusers.submit();
    }
    function deleteRecord()
    { //document.forms[0].submit();
        document.fom.action = "del.action";
        document.fom.submit();
    }
    function editr(val)
    {
        document.fom.action = "update.action?fid=" + val;
        document.fom.submit();
    }
     function show(val) {
        //var nakam = document.getElementById(%{#rowInd.index});
        document.getElementById('uName').value=val;
       // alert(val);
    }
    function submit(){
     //  refreshUsers.action="loadusers";
        //    refreshUsers.submit();
        document.formName.action = "tab1action";
        document.formName.submit();
        document.formName.action = "loadusers";
        document.formName.submit();
         document.formNamee.reset();
        //document.formName.submit();
        //alert(refreshUsers);
    }
</script>
<style type="text/css">
    table#viewid tr:nth-child(odd) {
        background-color: #7dc9e2;    
    }
    table#viewid tr:nth-child(even){
        background-color:   #009ae1; 
    }
    tbody { 

        overflow:scroll;
    }
   
</style> 

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <sd:head/>
    </head>
    <body >
 <jsp:include page="../Login/included.jsp" /> 
 <div>
            <div style=" display: table-cell;
                 vertical-align: top;
                 width: 43%;
                 height: 100%; float: left">
                <%-- <fieldset style="padding:5px;  height: 80%;"> --%>
                <div cssStyle="z-index: -1">
                <sd:tabbedpanel id="tab" useSelectedTabCookie="true" >     
                        <sd:div id="one" label="Create Users" theme="ajax" >
                            <s:form   action="tab1action" name="tab1action" method="post" id="refreshUsers">   
                                <div style=" position: absolute; left: 105px; width: 268px; height: 20px; background-color: greenyellow;">
                                    <s:property value="message"/> </div>
                                <table border = "0" id="createuser" class="user" font ="Tahoma">
                                    <tbody>
                                        <tr>
                                            <s:textfield name="username" label="UserName" size="40" />  
                                        </tr>
                                        <tr>
                                            <s:textfield name ="firstname" label="First Name" size="40" />
                                        </tr>
                                        <tr>
                                            <s:textfield name="lastname" label="Last Name" size="40"/>
                                        </tr>
                                        <tr>
                                    <p><s:textfield name="email" label="Email Address" size="40"/></p>
                                    </tr>
                                    <tr>
                                        <s:select label="User Type" name="type" style="width: 150px;" list="{'-Select-','Accountant','Administrator','Clerk','Teller'}"/>
                                    </tr>
                                    <tr>
                                        <s:reset name="clear" value="Clear Fields" style="width: 120px; height: 25px; " />
                                    </tr>
                                    <tr>
                                        <s:submit name="save" value ="Create User" style="width: 120px; height: 25px;" />

                                    </tr>
                                    </tbody>
                                </table>

                            </s:form>
                          
                        </sd:div>

                        <sd:div id="two" label="Reset Password" theme="ajax" >
                            <s:form   action="tab2action">
                                <s:submit />
                            </s:form>
                        </sd:div>

                        <sd:div id="three" label="Reset Account Status" theme="ajax" >
                            <s:form   action="tab2action">
                                <s:submit />
                            </s:form>
                        </sd:div>

                        <sd:div id="four" label="Enable/Disable Accounts" theme="ajax" >
                            <s:form   action="tab2action">
                                <h1>Enable/Disable Users</h1>
                                <sj:head jquery ="true" jquerytheme="redmond"/>
                                <s:url var="remoteurl" action="listusers"></s:url>
                                <s:url var="editurl" action="edit-grid-entry"></s:url>
                                    <sjg:grid
                                        id ="john"
                                        caption ="users"
                                        dataType="json"
                                        >
                                        <sjg:gridColumn name ="user" title="Username" editable="true" width="80"/>
                                    </sjg:grid>


                            </s:form>
                        </sd:div>

                    </sd:tabbedpanel>
               </div>
            </div>
          
                <div style="display: table-cell;
                     vertical-align: top;
                     width: 50% ;
                     height: 100% ;
                     bottom: 100px; "> 


                    <s:form action="loadusers" name="formName">
                        <legend>Registered Users</legend>
                         <button onclick="javascript:submit();">Load Users</button>
                        <table border="0" class="view" id="viewid" style="width: 100%; height: 100%;">
                            <form name="fom" method="post">
                                <tbody>
                                    <tr style="background-color:  #527881">
                                        <td>User ID</td>
                                        <td>User Name</td>
                                        <td>First Name</td>
                                        <td>Last Name</td>
                                        <td>Email Acc</td>
                                        <td>User Type</td> 
                                        <td>Action</td>
                                    </tr>
                                    <s:iterator value="list" status="listStatus">
                                        <tr>
                                            <td><s:property value="userid"/>
                                            <td><s:property value="username"/></td>
                                            <td><s:property value="firstname"/></td>
                                            <td><s:property value="lastname"/></td>
                                            <td><s:property value="emailacc"/></td>
                                            <td><s:property value="usertype"/></td>
                                            <td> <button class="my_popup_open" onclick="javascript:show('<s:property value="username"/>');" >Update</button> <button onclick="Manageusers.jsp">Delete</button> </td>

                                        </tr>
                                    </s:iterator>
                                </tbody>
                            
                        </table>
                    </s:form>
registered 
                </div> 
<div id="my_popup" style="position:absolute;margin:auto;left:35px;top:25px; 
background-color:#FFFFFF; display:none; height:440px; width:610px; border: 1px solid black;
opacity: 0.6;
filter: alpha(opacity=60);">
  <p>&nbsp;</p>
  <button class="my_popup_close" style="float: right"  >Close</button>
  <form>
     <table width="525" height="235" border="0" cellpadding="1" align="left">
         <tr> <td>User Name:</td><td><input type="text" id="uName"/> </td></tr>
	   <tr> <td>Email:</td><td><input type="text" /> </td></tr>
	    <tr> <td>First Name:</td><td><input type="text" /> </td></tr>
		 <tr> <td>Last Name:</td><td><input type="text" /> </td></tr>
		  <tr> <td height="51">User Type:</td>
		  <td><select name="type" style="size: auto"> <option>-Select-</option>  <option>Admin</option> <option>Accountant</option></select> </td></tr>
    </table>
  </form>
   <script>
                            $(document).ready(function () {

                                // Initialize the plugin
                                $('#my_popup').popup();

                            });
        </script>
</div> 
            
        </div>

        <!-- Include jQuery -->
        <script src="pop.js"></script>

        <!-- Include jQuery Popup Overlay -->
        <script src="pop_up_overlay.js"></script>

       
    </body>
</html> 