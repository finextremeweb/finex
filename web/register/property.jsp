<%-- 
    Document   : property
    Created on : Sep 15, 2015, 9:36:59 AM
    Author     : Simz
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sd" uri="/struts-dojo-tags"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Set Scheme Sizes</title>
        <sd:head/>

        <script type="text/javascript">
            function submit() {
                document.forms[0].submit();
            }

        </script>
        <style type="text/css">
            table#viewid tr:nth-child(odd) {
                background-color: #7dc9e2;    
            }
            table#viewid tr:nth-child(even){
                background-color:   #009ae1; 
            }   
        </style> 
    </head>
    <body>
 <fieldset  style="width:350px; float:  left" >
        <fieldset  style="width:350px">
            <legend style="background-color: transparent;" class="text-center">Property Registration</legend>

            <s:actionerror/>
            <s:property value="message"/>
            <s:form action="plot" name="plot">

                <table border = "0" id="contact" class="user" font ="Tahoma" cellpadding ="6">
                    <tbody>
                        <tr>
                            <s:select label="Category" 
                                      headerKey="1" headerValue="--- Select ---"
                                      list="{'Plot','Apartment', 'Others'}" 
                                      name="category" style="width: 250px; height: 25px;">  </s:select> 
                            <s:select label="Scheme Size" 
                                      headerKey="1" headerValue="--- Select ---"
                                      list="{1,2}" 
                                      name="schemeSize" style="width: 250px; height: 25px;">  </s:select> 

                            <s:textfield name="propertyNo" label="Property No" style="width: 250px; height: 25px;"></s:textfield>
                            <s:textfield name="propertyName" label="Property Name" style="width: 250px; height: 25px;"></s:textfield>

                            <s:select label="Property Size" 
                                      headerKey="1" headerValue="--- Select ---"
                                      list="{1,2}" 
                                      name="schemeSize" style="width: 250px; height: 25px;">  </s:select> 
                            <s:textfield  name="qoutedPrice" label="Quoted Price" style="width: 250px; height: 25px;"></s:textfield>
                            <s:submit name="save" value="Save" style="width: 150px; height: 30px;"/>
                            <s:reset name="clear" value="Clear Fields" style="width: 150px; height: 30px;"/>

                        </tr>
                    </tbody>
                </table> 
            </s:form> 
        </fieldset> 
 <fieldset  style="width:350px; float:  left">
  <legend style="background-color:  transparent;" class="text-center">Controls</legend>

            <s:form action="loadplotsize" name="loadplotsize">
                <table border="0" class="view" id="viewid" style="width: 100%; height: 100%;">

                    <tbody>
                        <tr style="background-color:  #527881">
                            <td>Scheme Sizes</td>
                        </tr>
                        <s:iterator value="list" status="listStatus">
                            <tr>
                                <td><s:property value="size"/></td>
                            </tr>
                        </s:iterator>
                    </tbody>
                </table>

            </s:form> 
</fieldset>
            
</fieldset> 
        <fieldset  style="width:60%; float:  left; position:  relative">
<legend style="background-color:  transparent; border: 2px" class="text-center">Registered Properties</legend>

            <s:form action="loadregproperties" name="loadplotsize">
                <table border="0" class="view" id="viewid" style="width: 100%; height: 100%;">

                    <tbody>
                        <tr style="background-color:  #527881">
                            <td>Scheme Sizes</td>
                        </tr>
                        <s:iterator value="list" status="listStatus">
                            <tr>
                                <td><s:property value="size"/></td>
                            </tr>
                        </s:iterator>
                    </tbody>
                </table>

            </s:form> 
        </fieldset>

    </body>
</html>