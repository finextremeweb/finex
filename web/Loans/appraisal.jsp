<%-- 
    Document   : appraisal
    Created on : Sep 23, 2015, 8:48:53 PM
    Author     : vinn
--%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sd" uri="/struts-dojo-tags"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*;" %>
<link href="assets/css/bootstrap-united.css" rel="stylesheet" />
<link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet" />
<script type="text/javascript">
    function load() {

        document.loadusers.submit();
    }
    function deleteRecord()
    { //document.forms[0].submit();
        document.fom.action = "del.action";
        document.fom.submit();
    }
    function editr(val)
    {
        document.fom.action = "update.action?fid=" + val;
        document.fom.submit();
    }
</script>
<style type="text/css">
    table#viewid tr:nth-child(odd) {
        background-color: #7dc9e2;    
    }
    table#viewid tr:nth-child(even){
        background-color:   #009ae1; 
    }
    tbody { 

        overflow:scroll;
    }
    legend {
        display: block;
        padding-left: 2px;
        padding-right: 2px;
        border: none;
    } 
</style> 

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Loan Appraisal</title>
        <sd:head/>
    </head>
    <body>

        <div>
            <div style=" display: table-cell;
                 vertical-align: top; padding-left: 100px;
                 width: 80%; padding-top: 100px;
                 height: 80%; padding-right: 100px;
                 float: left; padding-bottom: 100px;" >
                <%-- <fieldset style="padding:5px;  height: 80%;"> --%>
                    <sd:tabbedpanel id="tab" useSelectedTabCookie="true"  > 
                        <sd:div id="one" label="Member Info" theme="ajax"  > 
                            <div style=" position: absolute; left: 105px; 
                             width: 80%; height: 100%; padding-bottom: 50px;
                             padding-top: 50px;"></div>    
                             <table class="table">
				<form name="fom" method="post">
				<%	List l=(List)request.getAttribute("disp");
					if(l!=null)
					{					 
						Iterator it=l.iterator();					 
						while(it.hasNext())
						{				 
							com.finextreme.loans.Mybean b=(com.finextreme.loans.Mybean)it.next();
							int tempNum = b.getNo();
							String tempName = b.getNam();
							String tempCountry = b.getCt(); 
							%>
							<tr>
							<td><input type="checkbox" value="<%= tempNum %>" name="rdel"></td>
							<td><%= tempNum %></td>
							<td><%= tempName %></td>
							<td><%= tempCountry %></td>
							<td><a href="javascript:editr('<%= tempNum %>')">Edit</a></td>
							</tr>	 
							<%				 
						}	
					}				 
				%>
				<input type="button" value="delete" onclick="deleteRecord();">				 
				</form>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="2" style="padding-bottom:20px;"> 
                            <td align="left"> 
                            <form action="getMember" method="post" style="padding-top: 20px; 
                                  padding-left: 20px; padding-right: 20px;">
                                <fieldset>
                                    <legend
                                        style="font-size: 18px; font-weight: bold;
                                        color: brown; font-family: Georgia, serif;">
                                        Member Information </legend>
                                    <table border="0" cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td align="right"><font
                                                style="font-size: 15px;
                                                font-family: Arial, Times, serif; font-weight: bold">
                                                    Member Number:</font></td>
                                            <td><input name="myNumber" maxlength="25"
                                                size="30"/></td>
                                        </tr>
                                        <tr>
                                            <td align="right"><font
                                                style="font-size: 15px;
                                                font-family: Arial, Times, serif; font-weight: bold">
                                                    ID Number:</font></td>
                                            <td><input name="myId" maxlength="25"
                                                size="30" /></td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <input type="submit"
                                                name="btnSubmit" value="Search" /></td>                                        
                                        </tr>
                                    </table>
                                </fieldset>
                            </form>
                            </td>
                            <td align="right"> 
                            <form action="" method="post" style="padding-top: 20px; padding-left: 20px; 
                                  padding-right: 20px;">
                                <fieldset >
                                    <legend
                                        style="font-size: 18px; font-weight: bold;
                                        color: brown; font-family: Georgia, serif;">
                                        Employer Information </legend>
                                    <table border="0" cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td align="right"><font
                                                style="font-size: 15px;
                                                font-family: Arial, Times, serif; font-weight: bold">
                                                    Employer :</font>
                                            </td>
                                            <td><select name="type" style="size: auto"> <option>-Select-</option>  
                                                    <option>TSC</option> <option>Kenya Power</option></select> 
                                            </td></tr>
                                        </tr>
                                        <tr>
                                            <td align="right"><font
                                                style="font-size: 15px;
                                                font-family: Arial, Times, serif; font-weight: bold">
                                                    Payroll Number :</font></td>
                                            <td><input name="txtMobileNumber" maxlength="25"
                                                size="30"></td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </form>
                            </td>
                            </table>
                            
                            <form action="" method="post" style="padding-top: 20px; padding-left: 20px; 
                                  padding-right: 20px;">
                                <fieldset >
                                    <legend
                                        style="font-size: 18px; font-weight: bold;
                                        color: brown; font-family: Georgia, serif;">
                                        Member's Particulars </legend>
                                    <table border="0" cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td align="right"><font
                                                style="font-size: 15px;
                                                font-family: Arial, Times, serif; font-weight: bold">
                                                    Full Names :</font></td>
                                            <td><input name="txtEmailAddress" maxlength="30"
                                                size="30"></td>
                                            <td align="right"><font
                                                style="font-size: 15px;
                                                font-family: Arial, Times, serif; font-weight: bold">
                                                    Age :</font></td>
                                            <td><input name="age" maxlength="2"
                                                size="5"></td>
                                        </tr>
                                        <tr>
                                            <td align="right"><font
                                                style="font-size: 15px;
                                                font-family: Arial, Times, serif; font-weight: bold">
                                                    Work Station :</font></td>
                                            <td><input name="txtMobileNumber" maxlength="25"
                                                size="30"></td>
                                            <td align="right"><font
                                                style="font-size: 15px;
                                                font-family: Arial, Times, serif; font-weight: bold">
                                                    Loan Cycle :</font></td>
                                            <td><input name="loancyle" maxlength="2"
                                                size="5"></td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </form>
                            <table border="0" cellpadding="0" cellspacing="2"> 
                            <td align="left"> 
                            <form action="" method="post" style="padding-top: 20px; padding-left: 20px; 
                                  padding-right: 20px;" >
                                <table border="0" cellpadding="0" cellspacing="2"> 
                                    <tr>
                                        <td align="right">
                                            <fieldset>
                                                <legend
                                                    style="font-size: 18px; font-weight: bold;
                                                    color: brown; font-family: Georgia, serif;"> 
                                                    Shares </legend>
                                                <table border="0" cellpadding="3" cellspacing="0">
                                                    <tr>
                                                        <td align="right"><font
                                                            style="font-size: 15px;
                                                            font-family: Arial, Times, serif; font-weight: bold">
                                                                Total Shares :</font></td>
                                                        <td><input name="txtFirstName" maxlength="25" size="30">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td align="right"><font
                                                            style="font-size: 15px;
                                                            font-family: Arial, Times, serif; font-weight: bold">
                                                                Own Shares :</font></td>
                                                        <td><input name="txtLastName" maxlength="25" size="30">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td align="right"><font
                                                            style="font-size: 15px; font-family: Arial, Times, serif;
                                                             font-weight: bold">Committed Shares :</font>
                                                        </td>
                                                        <td><input type="password" name="txtLastName"
                                                            maxlength="25" size="30"></td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            </td>
                            <td align="right">
                                <table border="0" cellpadding="2" cellspacing="0">
                                    <tr>
                                        <td align="right" >
                                            <fieldset style="padding-top: 30px;">
                                                <legend
                                                    style="font-size: 18px; font-weight: bold; 
                                                    color: brown; font-family: Georgia, serif;">
                                                    Other Loans </legend>
                                                <table border="1" cellpadding="3" cellspacing="0">
                                                    <tr>
                                                        <td align="right"><font
                                                            style="font-size: 15px;
                                                            font-family: Arial, Times, serif; font-weight: bold">Email
                                                                Loan Serial Number</font></td>
                                                        <td align="right"><font
                                                            style="font-size: 15px;
                                                            font-family: Arial, Times, serif; font-weight: bold">Mobile
                                                                Loan Balance</font>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right"></td>
                                                        <td align="right">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right"></td>
                                                        <td align="right">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right"></td>
                                                        <td align="right">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right"></td>
                                                        <td align="right">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr> 
                                </table>
                            </td>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="2"> 
                            <td align="left"> 
                            <form action="" method="post" style="padding-top: 20px; padding-left: 20px; 
                                  padding-right: 20px;" >
                                <table border="0" cellpadding="0" cellspacing="2"> 
                                    <tr>
                                        <td align="right">
                                            <fieldset>
                                                <legend
                                                    style="font-size: 18px; font-weight: bold;
                                                    color: brown; font-family: Georgia, serif;"> 
                                                    Loan Information </legend>
                                                <table border="0" cellpadding="3" cellspacing="0">
                                                    <tr>
                                                        <td align="right"><font
                                                            style="font-size: 15px;
                                                            font-family: Arial, Times, serif; font-weight: bold">
                                                                Loan Type :</font></td>
                                                        <td><input name="txtFirstName" maxlength="25" size="30">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td align="right"><font
                                                            style="font-size: 15px;
                                                            font-family: Arial, Times, serif; font-weight: bold">
                                                                Loan Period :</font></td>
                                                        <td><input name="txtLastName" maxlength="25" size="30">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td align="right"><font
                                                            style="font-size: 15px; font-family: Arial, Times, serif;
                                                             font-weight: bold">Loan Date :</font>
                                                        </td>
                                                        <td><input type="password" name="txtLastName"
                                                            maxlength="25" size="30"></td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            </td>
                            <td align="right">
                                <table border="0" cellpadding="2" cellspacing="0">
                                    <tr>
                                        <td align="right" >
                                            <fieldset style="padding-top: 30px;">
                                                <legend
                                                    style="font-size: 18px; font-weight: bold; 
                                                    color: brown; font-family: Georgia, serif;">
                                                    Loan Amount </legend>
                                                <table border="0" cellpadding="3" cellspacing="0">
                                                    <tr>
                                                        <td align="right"><font
                                                            style="font-size: 15px;
                                                            font-family: Arial, Times, serif; font-weight: bold">
                                                                Applied Amount</font></td>
                                                        <td><input type="password" name="txtLastName"
                                                            maxlength="9" size="9"></td>
                                                        <td align="right"><font
                                                            style="font-size: 15px;
                                                            font-family: Arial, Times, serif; font-weight: bold">
                                                                Proposed Repayment Period (Months) </font>
                                                        </td>
                                                        <td><input type="password" name="txtLastName"
                                                            maxlength="3" size="5"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right"><font
                                                            style="font-size: 15px;
                                                            font-family: Arial, Times, serif; font-weight: bold">
                                                                 Proposed Repay</font></td>
                                                        <td><input type="password" name="txtLastName"
                                                            maxlength="7" size="9"></td>
                                                        <td align="right"><font
                                                            style="font-size: 15px;
                                                            font-family: Arial, Times, serif; font-weight: bold">
                                                                Proposed Instalmt Amount</font>
                                                        </td>
                                                        <td><input type="password" name="txtLastName"
                                                            maxlength="7" size="9"></td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td align="left">
                                            <input type="submit"
                                            name="btnSubmit" value="Submit" /></td>
                                    </tr> 
                                </table>
                            </td>
                            </table>
                            <table border="0" cellpadding="2" cellspacing="0">
                                <tr>
                                    <td align="left"><font
                                        style="font-size: 15px;
                                        font-family: Arial, Times, serif; font-weight: bold">Credit A/C :</font></td>
                                    <td><input name="txtEmailAddress" maxlength="30"
                                               size="35"></text></td>
                                    <td align="right"><font
                                        style="font-size: 15px;
                                        font-family: Arial, Times, serif; font-weight: bold">Mem FOSA Account:
                                            </font></td>
                                    <td><input name="txtMobileNumber" maxlength="30"
                                        size="35"></td>
                                </tr>
                                <tr>
                                    <td align="left"><font
                                        style="font-size: 15px;
                                        font-family: Arial, Times, serif; font-weight: bold">Purpose for Loan:</font></td>
                                    <td><input name="txtEmailAddress" maxlength="50"
                                               size="50"></text></td>
                                    <td align="right"><font
                                        style="font-size: 15px;
                                        font-family: Arial, Times, serif; font-weight: bold">Repayment Mode
                                            </font></td>
                                    <td><input name="txtMobileNumber" maxlength="8"
                                        size="8"></td>
                                </tr>
                            </table>
                        </sd:div>   
                                   
                        <sd:div id="two" label="PaySlip Capture" theme="ajax" >                          
                            <div style=" position: absolute; left: 105px; 
                             width: 80%; height: 100%; padding-bottom: 50px "></div>  
                            <form action="" method="post" style="padding-top: 20px; padding-left: 20px; 
                                  padding-right: 20px;" >
                                <table border="0" cellpadding="0" cellspacing="2"> 
                                    <tr>
                                        <td align="right">
                                            <fieldset>
                                                <legend
                                                    style="font-size: 18px; font-weight: bold;
                                                    color: brown; font-family: Georgia, serif;"> 
                                                    User Details </legend>
                                                <table border="0" cellpadding="3" cellspacing="0">
                                                    <tr>
                                                        <td align="right"><font
                                                            style="font-size: 15px;
                                                            font-family: Arial, Times, serif; font-weight: bold">First
                                                                Name:</font></td>
                                                        <td><input name="txtFirstName" maxlength="25" size="30">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td align="right"><font
                                                            style="font-size: 15px;
                                                            font-family: Arial, Times, serif; font-weight: bold">Last
                                                                Name:</font></td>
                                                        <td><input name="txtLastName" maxlength="25" size="30">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td align="right"><font
                                                            style="font-size: 15px; font-family: Arial, Times, serif;
                                                             font-weight: bold">Password:</font>
                                                        </td>
                                                        <td><input type="password" name="txtLastName"
                                                            maxlength="25" size="30"></td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" >
                                            <fieldset style="padding-top: 30px;">
                                                <legend
                                                    style="font-size: 18px; font-weight: bold; 
                                                    color: brown; font-family: Georgia, serif;">
                                                    Contact Details </legend>
                                                <table border="0" cellpadding="3" cellspacing="0">
                                                    <tr>
                                                        <td align="right"><font
                                                            style="font-size: 15px;
                                                            font-family: Arial, Times, serif; font-weight: bold">Email
                                                                Address:</font></td>
                                                        <td><input name="txtEmailAddress" maxlength="25"
                                                            size="30"></td>
                                                    </tr>

                                                    <tr>
                                                        <td align="right"><font
                                                            style="font-size: 15px;
                                                            font-family: Arial, Times, serif; font-weight: bold">Mobile
                                                                Number:</font></td>
                                                        <td><input name="txtMobileNumber" maxlength="25"
                                                            size="30"></td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td align="left">
                                            <input type="submit"
                                            name="btnSubmit" value="Submit" /></td>
                                    </tr> 
                                </table>
                            </form>                         
                            <table border="0" cellpadding="0" cellspacing="2"> 
                            <td align="left"> 
                            <form action="" method="post" style="padding-top: 20px; padding-left: 20px; 
                                  padding-right: 20px;" >
                                <table border="0" cellpadding="0" cellspacing="2"> 
                                    <tr>
                                        <td align="right">
                                            <fieldset>
                                                <legend
                                                    style="font-size: 18px; font-weight: bold;
                                                    color: brown; font-family: Georgia, serif;"> 
                                                    User Details </legend>
                                                <table border="0" cellpadding="3" cellspacing="0">
                                                    <tr>
                                                        <td align="right"><font
                                                            style="font-size: 15px;
                                                            font-family: Arial, Times, serif; font-weight: bold">First
                                                                Name:</font></td>
                                                        <td><input name="txtFirstName" maxlength="25" size="30">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td align="right"><font
                                                            style="font-size: 15px;
                                                            font-family: Arial, Times, serif; font-weight: bold">Last
                                                                Name:</font></td>
                                                        <td><input name="txtLastName" maxlength="25" size="30">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td align="right"><font
                                                            style="font-size: 15px; font-family: Arial, Times, serif;
                                                             font-weight: bold">Password:</font>
                                                        </td>
                                                        <td><input type="password" name="txtLastName"
                                                            maxlength="25" size="30"></td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            </td>
                            <td align="right">
                                <table border="0" cellpadding="2" cellspacing="0">
                                    <tr>
                                        <td align="right" >
                                            <fieldset style="padding-top: 30px;">
                                                <legend
                                                    style="font-size: 18px; font-weight: bold; 
                                                    color: brown; font-family: Georgia, serif;">
                                                    Contact Details </legend>
                                                <table border="0" cellpadding="3" cellspacing="0">
                                                    <tr>
                                                        <td align="right"><font
                                                            style="font-size: 15px;
                                                            font-family: Arial, Times, serif; font-weight: bold">
                                                                Address:</font></td>
                                                        <td><input name="txtEmailAddress" maxlength="25"
                                                            size="30"></td>
                                                    </tr>

                                                    <tr>
                                                        <td align="right"><font
                                                            style="font-size: 15px;
                                                            font-family: Arial, Times, serif; font-weight: bold">Mobile
                                                                Number:</font></td>
                                                        <td><input name="txtMobileNumber" maxlength="25"
                                                            size="30"></td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td align="left">
                                            <input type="submit"
                                            name="btnSubmit" value="Submit" /></td>
                                    </tr> 
                                </table>
                            </td>
                            </table>
                             <table border="0" cellpadding="3" cellspacing="0" 
                                    style="float: left; padding-bottom: 100px;">
                                <tr>
                                    <td align="right"><font
                                        style="font-size: 15px;
                                        font-family: Arial, Times, serif; font-weight: bold">Purpose for Loan:</font></td>
                                    <td><input name="txtEmailAddress" maxlength="50"
                                        size="50"></td>
                                </tr>
                                <tr>
                                    <td align="right"><font
                                        style="font-size: 15px;
                                        font-family: Arial, Times, serif; font-weight: bold">Repayment Mode
                                            </font></td>
                                    <td><input name="txtMobileNumber" maxlength="8"
                                        size="8"></td>
                                </tr>
                            </table>
                        </sd:div>                
                        <sd:div id="five" label="Guarantors" theme="ajax" >
                            <div style=" position: absolute; 
                                 width: 80%; height: 100%; padding-bottom: 50px"></div>
                            <s:property value="message"/> 
                            <div class="col-lg-5">
                            <h3>Section Two</h3>
                                <s:textfield label="Loan Grace Period" name="loanGracePeriod"
                                    cssClass="col-lg-12" placeholder="Loan Grace Period" />
                                <s:textfield label="Loan Round Off" name="loanRoundOff"
                                    cssClass="col-lg-12" placeholder="Loan Round Off" /> 
                            </div>
                        </sd:div>
                        <sd:div id="four" label="Report" theme="ajax" >
                            <div style=" position: absolute;
                                 width: 80%; height: 100%; padding-bottom: 50px"></div>
                            <s:property value="message"/> 
                            <div class="col-lg-5">
                            <h3>Section Two</h3>
                                <s:textfield label="Loan Grace Period" name="loanGracePeriod"
                                    cssClass="col-lg-12" placeholder="Loan Grace Period" />
                                <s:textfield label="Loan Round Off" name="loanRoundOff"
                                    cssClass="col-lg-12" placeholder="Loan Round Off" /> 
                            </div>
                        </sd:div>
                     
                    </sd:tabbedpanel>                             
               
            </div>
            
        </div>

        <!-- Include jQuery -->
        <script src="pop.js"></script>

        <!-- Include jQuery Popup Overlay -->
        <script src="pop_up_overlay.js"></script>

        <script>
            $(document).ready(function () {

                // Initialize the plugin
                $('#my_popup').popup();

            });
        </script>
        <script src="bootstrap/js/bootstrap.js"> </script>
    </body>
</html> 
