<%-- 
    Document   : appraisal
    Created on : Sep 23, 2015, 8:48:53 PM
    Author     : vinn
--%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sd" uri="/struts-dojo-tags"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*;" %>
<link href="assets/css/bootstrap-united.css" rel="stylesheet" />
<link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet" />
<script type="text/javascript">
    function load() {

        document.loadusers.submit();
    }
    function deleteRecord()
    { //document.forms[0].submit();
        document.fom.action = "del.action";
        document.fom.submit();
    }
    function editr(val)
    {
        document.fom.action = "update.action?fid=" + val;
        document.fom.submit();
    }
</script>
<style type="text/css">
    table#viewid tr:nth-child(odd) {
        background-color: #7dc9e2;    
    }
    table#viewid tr:nth-child(even){
        background-color:   #009ae1; 
    }
    tbody { 

        overflow:scroll;
    }
    legend {
        display: block;
        padding-left: 2px;
        padding-right: 2px;
        border: none;
    } 
</style> 

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Loan Appraisal</title>
        <sd:head/>
    </head>
    <body>

        <div>
            <div style=" display: table-cell;
                 vertical-align: top; padding-left: 100px;
                 width: 80%; padding-top: 100px;
                 height: 80%; padding-right: 100px;
                 float: left; padding-bottom: 100px;" >
                <%-- <fieldset style="padding:5px;  height: 80%;"> --%>
                    <sd:tabbedpanel id="tab" useSelectedTabCookie="true"  > 
                        <sd:div id="one" label="Member Info" theme="ajax"  > 
                           <s:form id="myForm" action="saveloantype" validate="true" cssClass="bs-example form-vertical"
                                    method="post">
                                    <fieldset>
                                        <div class="main col-lg-12 myHalfCol">
                                        <div class="col-lg-4">
                                         <h3>Section One</h3>
                                            <s:textfield label="Loan Type Code" name="loanTypeCode"
                                                    cssClass="col-lg-12" placeholder="Loan Type Code" />
                                            <s:textfield label="Loan Short Name" name="loanShortName"
                                                    cssClass="col-lg-12" placeholder="Loan Short Name" />			      
                                            <s:textfield label="Loan Type Name" name="loanTypeName"
                                                    cssClass="col-lg-12" placeholder="Loan Type Name" />
                                            <s:textfield label="Loan Serial Identifier" name="loanSerialIdentifier"
                                                    cssClass="col-lg-12" placeholder="Loan Serial Identifier" />			      
                                            <s:textfield label="Percentage" name="percentage"
                                                    cssClass="col-lg-12" placeholder="Percentage" />
                                            <s:textfield label="Loan Reducing Balance" name="loanReducingBalance"
                                                    cssClass="col-lg-12" placeholder="Loan Reducing Balance" />											
                                            <s:checkbox label="Uses Salary" name="usesSalary" fieldValue="true" 
                                                     placeholder="Uses Salary"></s:checkbox>
                                            <s:textfield label="Account Held in Trust" name="accHeldInTrust"
                                                    cssClass="col-lg-12" placeholder="Account Held in Trust" />
                                            <s:textfield label="prdCd" name="prdCd"
                                                    cssClass="col-lg-12" placeholder="prdCd" />
                                            <s:textfield label="Use Committed Shares" name="useCommittedShares"
                                                    cssClass="col-lg-12" placeholder="Use Committed Shares" />
                                            <s:textfield label="Clear Same Calendar Year" name="clearSameCalendarYear"
                                                    cssClass="col-lg-12" placeholder="Clear Same Calendar Year" />
                                            <s:textfield label="Class Code" name="classCode"
                                                    cssClass="col-lg-12" placeholder="Class Code" />											
                                            <s:checkbox label="recFromRefunds" name="recFromRefunds"
                                                    placeholder="recFromRefunds" fieldValue="true" />
                                            <s:checkbox label="Requires Salary" name="requiresSalary"
                                                    placeholder="Requires Salary" fieldValue="true" />
                                            <s:checkbox label="Rec From Sal" name="recFromSal"
                                                    placeholder="Rec From Sal" fieldValue="true" />
                                            <s:checkbox label="recFromCashDep" name="recFromCashDep"
                                                    placeholder="recFromCashDep" fieldValue="true" />
                                            <s:checkbox label="recFromChqs" name="recFromChqs"
                                                    placeholder="recFromChqs" fieldValue="true" />
                                            <s:checkbox label="recAuto" name="recAuto"
                                                    placeholder="recAuto" fieldValue="true" />
                                            <s:checkbox label="recoverFromDividend" name="recoverFromDividend"
                                                    placeholder="recoverFromDividend" fieldValue="true" />											
                                            <s:checkbox label="applyAffidavit" name="applyAffidavit"
                                                    placeholder="applyAffidavit" fieldValue="true" />
                                            <s:checkbox label="mustHaveSharesRegardless" name="mustHaveSharesRegardless"
                                                    placeholder="mustHaveSharesRegardless" fieldValue="true" />
                                            <s:checkbox label="fOSASchemeUsed" name="fOSASchemeUsed"
                                                    placeholder="fOSASchemeUsed" fieldValue="true" />										
                                            <s:textfield label="FOSA Scheme" name="fOSAScheme"
                                                    cssClass="col-lg-12" placeholder="FOSA Scheme" />
                                            <s:textfield label="FOSA Scheme Class Code" name="fOSASchemeClassCode"
                                                    cssClass="col-lg-12" placeholder="FOSA Scheme Class Code" />
                                            <s:textfield label="Take Guarantors AS" name="takeGuarantorsAS"
                                                    cssClass="col-lg-12" placeholder="Take Guarantors AS" />
                                            <s:textfield label="Maximum Number of Guarantors" name="maximumNumberOfGuarantors"
                                                    cssClass="col-lg-12" placeholder="Maximum Number of Guarantors" />											
                                            <s:textfield label="minimumNumberOfGuarantors" name="minimumNumberOfGuarantors"
                                                    cssClass="col-lg-12" placeholder="minimumNumberOfGuarantors" />	
                                            <s:checkbox label="splitFOSARepayments" name="splitFOSARepayments"
                                                    placeholder="splitFOSARepayments" fieldValue="true" />									</div>
                                            <div class="col-lg-4">
                                            <h3>Section Two</h3>								
                                                <s:textfield label="Loan Max Duration" name="loanMaxDuration"
                                                        cssClass="col-lg-12" placeholder="Loan Max Duration" />
                                                <s:textfield label="Loan Max Amount" name="loanMaxAmount"
                                                        cssClass="col-lg-12" placeholder="Loan Max Amount" />
                                                <s:textfield label="Loan Grace Period" name="loanGracePeriod"
                                                        cssClass="col-lg-12" placeholder="Loan Grace Period" />
                                                <s:textfield label="Loan Round Off" name="loanRoundOff"
                                                        cssClass="col-lg-12" placeholder="Loan Round Off" />
                                                <s:textfield label="Loan Grace Period" name="loanGracePeriod"
                                                        cssClass="col-lg-12" placeholder="Loan Grace Period" />
                                                <s:textfield label="Waiting Period" name="waitingPeriod"
                                                        cssClass="col-lg-12" placeholder="Waiting Period" />											
                                                <s:textfield label="Dividend Percent" name="dividendPerc" 
                                                        cssClass="col-lg-12" placeholder="Dividend Percent"/>
                                                <s:checkbox label="No Guarantors Required" name="noGuarantorsRequired"
                                                        placeholder="No Guarantors Required" fieldValue="true" />
                                                <s:checkbox label="No Deduction Required" name="noDeductionRequired"
                                                        placeholder="No Deduction Required" fieldValue="true" />
                                                <s:checkbox label="Product Closed" name="productClosed"
                                                        placeholder="Product Closed" fieldValue="true" />	
                                                <s:checkbox label="Ignore Salary Computation if Blank" name="ignoreSalComputationIfBlank"
                                                        placeholder="Ignore Salary Computation if Blank" fieldValue="true" />	
                                                <s:checkbox label="Is Fosa Loan" name="isFosaLoan"
                                                        placeholder="Is Fosa Loan" fieldValue="true" />
                                                <s:checkbox label="Is Fosa Advance" name="isFosaAdvance"
                                                        placeholder="Is Fosa Advance" fieldValue="true" />
                                                <s:checkbox label="Is Intr Cat Defined" name="isIntrCatDefined"
                                                        placeholder="Is Intr Cat Defined" fieldValue="true" />
                                                <s:checkbox label="Requires Salary" name="requiresSalary"
                                                        placeholder="Requires Salary" fieldValue="true" />
                                                <s:checkbox label="Rec From Sal" name="recFromSal"
                                                        placeholder="Rec From Sal" fieldValue="true" />
                                                <s:checkbox label="recFromCashDep" name="recFromCashDep"
                                                        placeholder="recFromCashDep" fieldValue="true" />
                                                <s:checkbox label="recFromChqs" name="recFromChqs"
                                                        placeholder="recFromChqs" fieldValue="true" />

                                                <s:textfield label="Instant Int Last Date" name="instantIntLastDate"
                                                        cssClass="col-lg-12" placeholder="Instant Int Last Date" />	
                                                <s:checkbox label="No Monthly Int" name="noMonthlyInt"
                                                        placeholder="No Monthly Int" fieldValue="true" />
                                                <s:checkbox label="recPrincAsPT" name="recPrincAsPT"
                                                        placeholder="recPrincAsPT" fieldValue="true" />
                                                <s:checkbox label="Charge Interest Immediately" name="chargeIntImmediately"
                                                        placeholder="Charge Interest Immediately" fieldValue="true" />
                                                <s:checkbox label="Allow Top Up" name="allowTopUp"
                                                        placeholder="Allow Top Up" fieldValue="true" />
                                                <s:checkbox label="Is Refinance Loan" name="isRefinanceLoan"
                                                        placeholder="Is Refinance Loan" fieldValue="true" />
                                                <s:checkbox label="Rec Dividends" name="recDividends"
                                                        placeholder="Rec Dividends" fieldValue="true" />
                                                <s:checkbox label="Debit Loan With Total Interest On Posting" name="debitLoanWithTotalInterestOnPosting"
                                                        placeholder="Debit Loan With Total Interest On Posting" fieldValue="true" />
                                            </div>
                                            <div class="col-lg-4">
                                            <h3>Section Three</h3>								
                                                <s:textfield label="Loan Account" name="loanAccount"
                                                        cssClass="col-lg-12" placeholder="Loan Account" />
                                                <s:textfield label="Commission" name="commission"
                                                        cssClass="col-lg-12" placeholder="Commission" />
                                                <s:textfield label="Interest Account" name="interestAccount"
                                                        cssClass="col-lg-12" placeholder="Interest Account" />
                                                <s:textfield label="Repay Formular" name="repayFormular"
                                                        cssClass="col-lg-12" placeholder="Repay Formular" />
                                                <s:textfield label="Loan Used" name="loanUsed"
                                                        cssClass="col-lg-12" placeholder="Loan Used" />
                                                <s:textfield label="Scheme Used" name="schemeUsed"
                                                        cssClass="col-lg-12" placeholder="Scheme Used" />																						
                                                <s:checkbox label="MFI Loan" name="mFILoan" fieldValue="true" 
                                                        placeholder="MFI Loan"></s:checkbox>
                                                <s:checkbox label="Recomend Default" name="recomendDefault"
                                                        placeholder="Recomend Default" fieldValue="true"  />
                                                <s:textfield label="Maximum Loan Type" name="maxLoanType"
                                                        cssClass="col-lg-12" placeholder="Maximum Loan Type" />
                                                <s:textfield label="Salary Percentage" name="salaryPerc"
                                                        cssClass="col-lg-12" placeholder="salaryPerc" />
                                                <s:textfield label="Minimum Interest" name="minInterest"
                                                        cssClass="col-lg-12" placeholder="Minimum Interest" />
                                                <s:checkbox label="Uses Dividends" name="usesDividends"
                                                        placeholder="Uses Dividends" fieldValue="true" />
                                                <s:checkbox label="Constant Int" name="constantInt"
                                                        placeholder="Constant Int" fieldValue="true" />	
                                                <s:checkbox label="Recomend Dormant" name="recomendDormant"
                                                        placeholder="Recomend Dormant" fieldValue="true" />	
                                                <s:checkbox label="Allow Loann Of Same Type" name="allowLnOfSameType"
                                                        placeholder="Allow Loann Of Same Type" fieldValue="true" />	
                                                <s:textfield label="Account Credit Liability" name="accCreditLiab"
                                                        cssClass="col-lg-12" placeholder="Account Credit Liability" />																						
                                                </div>
                                            </div>	
                                            <div class="col-lg-8 col-lg-offset-4">
                                                    <s:submit cssClass="btn btn-default col-lg-2" value="Cancel" />
                                                    <s:submit cssClass="btn btn-primary col-lg-2 col-lg-offset-1" value="Save" />									
                                                    <!-- <s:hidden name="pageName" value="login" /> -->
                                            </div>
                                    </fieldset>
                            </s:form> 
                            
                        </sd:div>
                     
                    </sd:tabbedpanel>                             
               
            </div>
            
        </div>

        <!-- Include jQuery -->
        <script src="pop.js"></script>

        <!-- Include jQuery Popup Overlay -->
        <script src="pop_up_overlay.js"></script>

        <script>
            $(document).ready(function () {

                // Initialize the plugin
                $('#my_popup').popup();

            });
        </script>
        <script src="bootstrap/js/bootstrap.js"> </script>
    </body>
</html> 
