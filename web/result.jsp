<%-- 
    Document   : result
    Created on : Sep 10, 2015, 12:41:27 PM
    Author     : Simz
--%>

<%@ taglib prefix="s" uri="/struts-tags" %>
<html>

<body>
<h1>Struts 2 example</h1>

<h2>
  Favor fruit : <s:property value="yourFruits"/>
</h2> 

<h2>
  Selected month : <s:property value="yourMonth"/>
</h2> 

</body>
</html>
