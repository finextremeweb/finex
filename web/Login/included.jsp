<%-- 
    Document   : Mainmenu
    Created on : Sep 1, 2015, 1:58:51 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!doctype html>
<!doctype html>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
    <head>
        <meta charset='utf-8'>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../css/styles.css">
        <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
        <script src="javascript/script.js"></script>
        <title>Main Menu</title>
    </head>
    <body style="margin-top:0">

        <div id='cssmenu'>
            <ul>
                <li><a href='#'> <b>Summary </b></a></li>
                <li><a href='#'><b>Company</b></a></li>
                <li><a href='#'><b>Members</b></a>
                    <ul>
                        <li><a href='#'>Registration</a></li>
                    </ul>
                </li>
                <li><a href='#'> <b>Property Management</b></a>
                    <ul>
                        <li class='has-sub'><a href='#'><b>Registration </b></a>
                            <ul>
                                <li><a href="../register/scheme.jsp" >Scheme</a></li>
                               <li><a href="../register/property.jsp" >Property</a></li>
                            </ul>
                        </li>
                        <li class='has-sub'><a href='#'>Property Transfer </a></li>
                    </ul>
                </li>
                <li><a href='#'><b>Security</b></a>
                    <ul>
                        <li><a href='#'>Login Trail</a>
                        <li class=""><a href="<s:url action="loadusers"/>" >User Manager</a></li>
                        <li><a href='#'>Change Password</a></li>
                        <li><a href='#'>Users Connected</a></li>
                    </ul>  </li>
                <li><a href='#'><b>Accounts</b></a></li>
                <li><a href='#'><b>System parameters</b></a></li>
                <li><a href='#'><b>Reports</b></a>
                    <ul>
                        <li class='has-sub'><a href='#'> <b>Financial Report</b></a>
                            <ul>
                                <li><a href='#'>Summary By Account</a></li>
                                <li><a href='#'>Summary By Property</a></li>
                                <li><a href='#'>Income Statement</a></li>
                                <li><a href='#'>Expense Statement</a></li> 
                            </ul>
                        </li>
                        <li class='has-sub'><a href='#'> <b>Members Report</b></a>
                            <ul>
                                <li><a href='#'>All Members</a></li>
                                <li><a href='#'>members Contact List</a></li>   
                            </ul>
                        </li> 
                    </ul>
                </li>
                <li><a href='#'> <b>Tools</b></a>
                    <ul>
                        <li><a href='#'><br>Calculator</a></li>
                    </ul>
                </li>   
                <li><a href='#'><b>Help</b></a></li>
                <li><a href='#' > User <s:property value=" username" /></a>
                <ul>
                        <li><a href='#'><br>Sign Out</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </body>
</html>

