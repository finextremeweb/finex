<%-- 
    Document   : login
    Created on : Sep 2, 2015, 1:30:30 PM
    Author     : ADMIN
--%>

<%@page import="java.util.Calendar"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sd" uri="/struts-dojo-tags"%>
<html>
    <head>
         <s:head/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login form</title>
         <link rel="stylesheet" href="../css/style.css">
         <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
         <section class="container">
    <div class="login">
      <h1> Property Management System</h1>
        <s:actionerror/>
        <s:form method="post" action="login">
            <p><s:textfield name="username" placeholder="UserName"/></p>
            <p><s:password name="password" placeholder="Password"/></p>
            <p> <s:submit value="Login"/></p> 
        </s:form>       
  </section>
        <div class="login-help">
      <p>Forgot your password? <a href="login.jsp">Click here to reset it</a>.</p>
    </div> 
        <div id="footer" style="text-align: center">
                &copy; <%=Calendar.getInstance().get(Calendar.YEAR)%>, All Rights Reserved . 
                    <br/>
                </div>
        
        
    </body>
</html>
