<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" %>



<script type="text/javascript">
    function show(val) {
        //var nakam = document.getElementById(%{#rowInd.index});
        
        alert(val);
    }
    function filter() {
        // alert("vh");
        document.forms['logtableId'].action = 'logtable';
        document.forms['logtableId'].submit();
    }
</script>



<html>
    <head>
        <title>Displaying Values From Database |</title>
        <style type="text/css">
            table#viewid tr:nth-child(odd) {
                background-color: #7dc9e2;    
            }
            table#viewid tr:nth-child(even){
                background-color:   #009ae1; 
            }   
        </style> 

    </head>
    
    <body  style="margin-top:0">
       <jsp:include page="../Login/included.jsp" /> 
        <div>
            <s:form>
                <s:submit value="Print"></s:submit>
            </s:form>
            <s:form action="find" id="findId"></s:form>
            </div>

            <input type="text" name="filtered" value="" onkeyup="javascript:filter();
                " />

        <s:form action="logtable" id="logtableId">
            <table border="0" class="view" id="viewid" style="width: 100%; height: 100%;">
                <tr style="background:  #ffffff; background-color:  #527881" >
                    <th>##</th>
                    <th >Username</th>
                    <th>Log ID</th>
                    <th>Log In Time</th>
                    <th>Log Out Time</th>
                    <th>Time Spent(Mins)</th>  
                </tr>

                <s:iterator value="list" status="rowInd">

                    <tr id="iRow<s:property value="username"/>">
                        
                        <%--<s:checkbox onclick="deleteRow(%{#rowInd.index})"/>--%>
                        <td> <button onclick="javascript:show('<s:property value="username"/>');"></button></td>
                        <td><s:property value="username"/> </td>
                        <td><s:property value="LogId"/></td>
                        <td><s:property value="timelogged"/></td>
                        <td><s:property value="datelogged"/></td>
                        <td><s:property value="timespent"/></td>


                    </tr>
                </s:iterator>
            </table>

        </body>
    </s:form>
</html>