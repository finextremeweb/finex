<%-- 
    Document   : combobox
    Created on : Sep 10, 2015, 12:33:06 PM
    Author     : Simz
--%>

<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
    <head>
    </head>

    <body>
        <h1>Struts 2 Combo Box example</h1>

            <s:form action="resultAction" namespace="/">

                <h2>
                    <s:combobox label="What's your favor fruit" 
                                headerKey="-1" headerValue="--- Select ---"
                                list="fruits" 
                                name="yourFruits" ></s:combobox>
                </h2>

                <h2>
                    
                </h2> 

                <s:submit value="submit" name="submit" />

            </s:form>

        </body>
    </html>

